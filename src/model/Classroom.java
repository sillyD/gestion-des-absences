package model;
/**
 * Representing a classroom
 * 
 * @author DIOP-SONY
 * @version 1.0
 */
public class Classroom {

	/**
	 * the classname
	 */
	String name;
	/**
	 * the classtype
	 */
	String type;
	
	/**
	 * Constructor
	 * 
	 * @param name the class name
	 * @param type the class type
	 */
	public Classroom(String name, String type) {
		this.name = name;
		this.type = type;
	}
	/**
	 * getter for the name
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**setter for the name
	 * 
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * getter for the type
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * setter for the type
	 * @param type the new type
	 */
	public void setType(String type) {
		this.type = type;
	}
	
}
