package model;
/**
 * Super classe d'instancation des utilisateurs
 * 
 * @author DIOP-SONY
 * @version 1.0
 */
public abstract class User {

	/**
	 * identifiant de l'utilisateur
	 */
	protected int id;
	/**
	 * nom de l'utilisateur
	 */
	protected String lastName;
	/**
	 * prenom de l'utilisateur
	 */
	protected String firstName;
	/**
	 * mail de l'utilisateur
	 */
	protected String mail;
	/**
	 * mot de passe de l'utilisateur
	 */
	protected String password;
	
	/**
	 * Constructor of the super class User
	 * 
	 * @param id user's identifier
	 * @param lastName user's lastName
	 * @param firstName user's firstName
	 * @param mail user's mail
	 * @param password user's password
	 */
	public User(int id, String lastName, String firstName, String mail, String password) {
		this.id = id;
		this.lastName = lastName;
		this.firstName = firstName;
		this.mail = mail;
		this.password = password;
	}
	/**
	 * Second Constructor of the super class User
	 * 
	 * @param lastName user's lastName
	 * @param firstName user's firstName
	 * @param mail user's mail
	 * @param password user's password
	 */
	public User(String lastName, String firstName, String mail, String password) {
		this.lastName = lastName;
		this.firstName = firstName;
		this.mail = mail;
		this.password = password;
	}
	/**
	 * getter for the attribute id
	 * @return the id
	 */
	public abstract int getId();
	/**
	 * setter for the attribute id
	 * @param newId the new Id
	 */
	public abstract void setId(int newId);
	/**
	 * getter for the attribute lastName
	 * @return the lastName
	 */
	public abstract String getLastName();
	/**
	 * setter for the attribute lastName
	 * @param newLastName the new lastName
	 */
	public abstract void setLastName(String newLastName);
	/**
	 * getter for the attribute firstName
	 * @return the firstName
	 */
	public abstract String getFirstName();
	/**
	 * setter for the attribute firstName
	 * @param newFirstName the new firstName
	 */
	public abstract void setFirstName(String newFirstName);
	/**
	 * getter for the attribute mail
	 * @return the mail
	 */
	public abstract String getMail();
	/**
	 * setter for the attribute mail
	 * @param newMail the new mail
	 */
	public abstract void setMail(String newMail);
	/**
	 * getter for the attribute password
	 * @return the password
	 */
	public abstract String getPassword();
	/**
	 * setter for the attribute password
	 * @param newPassword the new password
	 */
	public abstract void setPassword(String newPassword);
}
