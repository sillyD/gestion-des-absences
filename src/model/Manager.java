package model;
/**
 * Manager class
 * 
 * @author DIOP-SONY
 * @version 1.0 
 */
public class Manager extends User {

	/**
	 * the phone number
	 */
	private String phone;
	
	/**
	 * Constructor
	 * 
	 * @param id manager's identifier
	 * @param lastName manager's lastName
	 * @param firstName manager's firstName
	 * @param mail manager's mail
	 * @param password manager's password
	 * @param phone manager's phone
	 */
	public Manager(int id, String lastName, String firstName, String mail, String password, String phone) {
		super(id, lastName, firstName, mail, password);
		this.phone = phone;
	}
	
	@Override
	public int getId() {
		// TODO Auto-generated method stub
		return super.id;
	}
	@Override
	public void setId(int newId) {
		// TODO Auto-generated method stub
		super.id = newId;
	}
	@Override
	public String getLastName() {
		// TODO Auto-generated method stub
		return super.lastName;
	}

	@Override
	public void setLastName(String newLastName) {
		// TODO Auto-generated method stub
		super.lastName = newLastName;
	}

	@Override
	public String getFirstName() {
		// TODO Auto-generated method stub
		return super.firstName;
	}

	@Override
	public void setFirstName(String newFirstName) {
		// TODO Auto-generated method stub
		super.firstName = newFirstName;
	}
	@Override
	public String getMail() {
		// TODO Auto-generated method stub
		return super.mail;
	}
	@Override
	public void setMail(String newMail) {
		// TODO Auto-generated method stub
		super.mail = newMail;
	}
	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return super.password;
	}
	@Override
	public void setPassword(String newPassword) {
		// TODO Auto-generated method stub
		super.password = newPassword;
	}
	/**
	 * getter pour l'attribut phone
	 * @return le telephone du gestionnaire
	 */
	public String getPhone() {
		return phone;
	}
	/**
	 * setter pour l'attribut phone
	 * @param newPhone le nouveau telephone du gestionnaire
	 */
	public void setPhone(String newPhone) {
		this.phone = newPhone;
	}

	

}
