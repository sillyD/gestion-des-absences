package model;
/**
 * Session class
 * 
 * @author DIOP-SONY
 * @version 1.0
 */
public class Session {

	/**
	 * session's id
	 */
	private String id;
	/**
	 * date of the session
	 */
	private String date;
	/**
	 * timetable of the session
	 */
	private String timetable;
	/**
	 * duration of the session
	 */
	private int duration;
	/**
	 * session type
	 */
	private String type;
	/**
	 * unit referring to the session
	 */
	private String unitName;
	/**
	 * teacher name 
	 */
	private int teacherId;
	/**
	 * the number of the group
	 */
	private String groupNumber;
	/**
	 * the classroom
	 */
	private String classroom;
	/**
	 * the number of absence
	 */
	private int absencesNumber;
	/**
	 * the session's status
	 */
	private String status;
	
	/**
	 * Constructor of the session class
	 * 
	 * @param id the session's ID
	 * @param date session date
	 * @param timetable the session timetable
	 * @param duration the session duration
	 * @param type the session type
	 * @param unitName the session unitName
	 * @param teacherId the Teacher ID
	 * @param groupNumber the students group attending in the session
	 * @param classroom the class of the session
	 * @param absencesNumber the number of absences in the session
	 * @param status the session's status
	 */
	public Session(String id, String date, String timetable, int duration, String type, String unitName, int teacherId, String groupNumber, String classroom, int absencesNumber, String status) {
		this.id = id;
		this.date = date;
		this.timetable = timetable;
		this.duration = duration;
		this.type = type;
		this.unitName = unitName;
		this.teacherId = teacherId;
		this.groupNumber = groupNumber;
		this.classroom = classroom;
		this.absencesNumber = absencesNumber;
		this.status = status;
	}
	/**
	 * getter for the session's id
	 * @return the session's id
	 */
	public String getId() {
		return id;
	}
	/**
	 * setter for the session'id 
	 * @param newId the new id
	 */
	public void setId(String newId) {
		id = newId;
	}
	/**
	 * getter for the session date
	 * @return the session date
	 */
	public String getDate() {
		return date;
	}
	/**
	 * setter for the session date
	 * @param newDate the new session date
	 */
	public void setDate(String newDate) {
		date = newDate;
	}
	/**
	 * getter for the session timetable
	 * @return the session timetable
	 */
	public String getTimetable() {
		return timetable;
	}
	/**
	 * setter for the session timetable
	 * @param newTimetable the new session timetable 
	 */
	public void setTimetable(String newTimetable) {
		timetable = newTimetable;
	}
	/**
	 * getter for the session duration
	 * @return the session duration
	 */
	public int getDuration() {
		return duration;
	}
	/**
	 * setter for the session duration
	 * @param newDuration the new session duration
	 */
	public void setDuration(int newDuration) {
		duration = newDuration;
	}
	/**
	 * getter for the session type
	 * @return the session type
	 */
	public String getType() {
		return type;
	}
	/**
	 * setter for the session type
	 * @param newType the new session type
	 */
	public void setType(String newType) {
		type = newType;
	}
	/**
	 * getter for the unitName refereed to the station
	 * @return the unitName refereed to the session
	 */
	public String getUnitName() {
		return unitName;
	}
	/**
	 * setter for the unitName
	 * @param newUnitName the new unitName
	 */
	public void setUnitName(String newUnitName) {
		unitName = newUnitName;
	}
	/**
	 * getter for the teacher id
	 * @return the teacher id
	 */
	public int getTeacherId() {
		return teacherId;
	}
	/**
	 * setter for the teacher id
	 * @param newTeacherId the new teacher id
	 */
	public void setTeacherId(int newTeacherId) {
		teacherId = newTeacherId;
	}
	/**
	 * getter for the group Number
	 * @return the teacher mail
	 */
	public String getGroupNumber() {
		return groupNumber;
	}
	/**
	 * setter for the group number
	 * @param newGroupNumber the new group number
	 */
	public void setGroupNumber(String newGroupNumber) {
		groupNumber = newGroupNumber;
	}
	/**
	 * getter for the classroom
	 * @return the classroom 
	 */
	public String getClassroom() {
		return classroom;
	}
	/**
	 * setter for the classroom
	 * @param newClassroom the new classroom
	 */
	public void setClassroom(String newClassroom) {
		classroom = newClassroom;
	}
	/**
	 * getter for the number of absences
	 * @return the number of absences
	 */
	public int getAbsencesNumber() {
		return absencesNumber;
	}
	/**
	 * setter for the number of absences
	 * @param newAbsencesNumber the new number of absences
	 */
	public void setAbsencesNumber(int newAbsencesNumber) {
		absencesNumber = newAbsencesNumber;
	}
	/**
	 * getter for the session's status
	 * @return the session's status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * setter for the session's status
	 * @param newStatus the new session's status
	 */
	public void setStatus(String newStatus) {
		status = newStatus;
	}
}
