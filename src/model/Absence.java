package model;
/**
 * Absence class 
 * 
 * @author DIOP-SONY
 * @version 1.0
 *
 */
 
public class Absence {

	/**
	 * absence date
	 */
	private String date;
	/**
	 * absence type
	 */
	private String type;
	
	/**
	 * Constructor
	 * 
	 * @param date absence date
	 * @param type absence type
	 */
	public Absence(String date, String type) {
		this.date = date;
		this.type = type;
	}
	/**
	 * Constructor
	 * 
	 * @param date absence date
	 */
	public Absence(String date) {
		this.date = date;
	}
	/**
	 * getter for the absence date
	 * @return the absence date
	 */
	public String getDate() {
		return date;
	}
	/**
	 * setter for the absence date
	 * @param newDate the new absence date
	 */
	public void setDate(String newDate) {
		date = newDate;
	}
	/**
	 * getter for the absence type
	 * @return the absence type
	 */
	public String getTypeAbsence() {
		return type;
	}
	/**
	 * setter for the absence type
	 * @param newType the new absence type
	 */
	public void setTypeAbsence(String newType) {
		type = newType;
	}
	
}
