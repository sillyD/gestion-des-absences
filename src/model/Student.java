package model;

/**
 * Classe Etudiant fille de la classe User
 * 
 * @author DIOP-SONY
 * @version 1.0
 */
public class Student extends User{

	/**
	 * student's sector
	 */
	private String sector;
	/**
	 * student's UnitGroupNumber
	 */
	private String unitGroupNumber;
	/**
	 * student's EnglishGroupNumber
	 */
	private String englishGroupNumber;
	/**
	 * student's ExamGroupNumber
	 */
	private String examGroupNumber;
	/**
	 * student's absenceHours
	 */
	private int absencesHours;
	
	/**
	 * Constructor
	 * 
	 * @param lastName student's lastName
	 * @param firstName student's firstName
	 * @param mail student's mail
	 * @param password student's password
	 * @param sector student's sector
	 * @param unitGroupNumber the unit group number
	 * @param englishGroupNumber the english group number
	 * @param examGroupNumber the exam group number
	 * @param absencesHours the absences hours
	 */
	public Student(String lastName, String firstName, String mail, String password, String sector, String unitGroupNumber, String englishGroupNumber, String examGroupNumber, int absencesHours) {
		super(lastName, firstName, mail, password);
		this.sector = sector;
		this.unitGroupNumber = unitGroupNumber;
		this.englishGroupNumber = englishGroupNumber;
		this.examGroupNumber = examGroupNumber;
		this.absencesHours = absencesHours;
	}
	
	@Override
	public int getId() {
		// TODO Auto-generated method stub
		return super.id;
	}
	@Override
	public void setId(int newId) {
		// TODO Auto-generated method stub
		super.id = newId;
	}
	@Override
	public String getLastName() {
		// TODO Auto-generated method stub
		return super.lastName;
	}

	@Override
	public void setLastName(String newLastName) {
		// TODO Auto-generated method stub
		super.lastName = newLastName;
	}

	@Override
	public String getFirstName() {
		// TODO Auto-generated method stub
		return super.firstName;
	}

	@Override
	public void setFirstName(String newFirstName) {
		// TODO Auto-generated method stub
		super.firstName = newFirstName;
	}
	@Override
	public String getMail() {
		// TODO Auto-generated method stub
		return super.mail;
	}
	@Override
	public void setMail(String newMail) {
		// TODO Auto-generated method stub
		super.mail = newMail;
	}
	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return super.password;
	}
	@Override
	public void setPassword(String newPassword) {
		// TODO Auto-generated method stub
		super.password = newPassword;
	}
	/**
	 * getter for the sector
	 * @return the sector
	 */
	public String getSector() {
		return sector;
	}
	/**
	 * setter for the sector
	 * @param newSector the new sector
	 */
	public void setFiliere(String newSector) {
		this.sector = newSector;
	}
	/**
	 * getter for the unit group number
	 * @return the unit group number
	 */
	public String getUnitGroupNumber() {
		return unitGroupNumber;
	}
	/**
	 * setter for the unit group number
	 * @param newUnitGroupNumber the new unit group number
	 */
	public void setUnitGroupNumber(String newUnitGroupNumber) {
		unitGroupNumber = newUnitGroupNumber;
	}
	/**
	 * getter for the english group number
	 * @return the english group number
	 */
	public String getEnglishGroupNumber() {
		return englishGroupNumber;
	}
	/**
	 * setter for the english group number
	 * @param newEnglishGroupNumber the new unit group number
	 */
	public void setEnglishGroupNumber(String newEnglishGroupNumber) {
		englishGroupNumber = newEnglishGroupNumber;
	}
	/**
	 * getter for the exam group number
	 * @return the exam group number
	 */
	public String getExamGroupNumber() {
		return examGroupNumber;
	}
	/**
	 * setter for the exam group number
	 * @param newExamGroupNumber the new exam group number
	 */
	public void setExamGroupNumber(String newExamGroupNumber) {
		examGroupNumber = newExamGroupNumber;
	}
	/**
	 * getter for the absences hours
	 * @return the absences hours
	 */
	public int getAbsencesHours() {
		return absencesHours;
	}
	/**
	 * setter for the absences hours
	 * @param newAbsencesHours the new absences hours
	 */
	public void setAbsencesHours(int newAbsencesHours) {
		absencesHours = newAbsencesHours;
	}
}
