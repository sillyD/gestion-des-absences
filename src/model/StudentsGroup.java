package model;

/**
 * Class StudentsGroup
 * 
 * @author DIOP-SONY
 * @version 1.0
 */
public class StudentsGroup {

	/**
	 * the group number
	 */
	protected String number;
	/**
	 * the group type
	 */
	protected String type;
	/**
	 * the planning of the group
	 */
	protected String planning;
	
	/**
	 * Constructor
	 * 
	 * @param number the groupNumber
	 * @param type the group type
	 * @param planning the planning of the group
	 */
	public StudentsGroup(String number, String type, String planning) {
		this.number = number;
		this.type = type;
		this.planning = planning;
	}
	/**
	 * getter for the number
	 * @return the group number
	 */
	public String getNumber() {
		return number;
	}
	/**
	 * setter for the number
	 * @param newNumber the new number
	 */
	public void setNumero(String newNumber) {
		number = newNumber;
	}
	/**
	 * getter for the type
	 * @return the group type
	 */
	public String getType() {
		return type;
	}
	/**
	 * setter for the type
	 * @param newType the new type 
	 */
	public void setType(String newType) {
		type = newType;
	}
	/**
	 * getter for the planning
	 * @return the planning
	 */
	public String getPlanning() {
		return planning;
	}
	/**
	 * setter for the planning
	 * @param newPlanning the new planning
	 */
	public void setPlanning(String newPlanning) {
		planning = newPlanning;
	}
}
