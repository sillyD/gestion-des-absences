package model;

/**
 * Unit class
 * 
 * @author DIOP-SONY
 * @version 1.0
 */
public class Unit {

	/**
	 * unit name
	 */
	private String name;
	/**
	 * the hourly mass
	 */
	private int hourlyMass;
	/**
	 * TD hours
	 */
	private int hoursTD;
	/**
	 * TP hours
	 */
	private int hoursTP;
	/**
	 * AMPHI hours
	 */
	private int hoursAMPHI;
	/**
	 * EXAM hours
	 */
	private int hoursEXAM;
	
	/**
	 * Constructor
	 * 
	 * @param name the unit name
	 * @param hourlyMass the hourlyMass
	 * @param hoursTD hours of TD
	 * @param hoursTP hours of TP
	 * @param hoursAMPHI hours of AMPHI
	 * @param hoursEXAM hours of EXAM
	 */
	public Unit(String name, int hourlyMass, int hoursTD, int hoursTP, int hoursAMPHI, int hoursEXAM) {
		this.name = name;
		this.hourlyMass = hourlyMass;
		this.hoursTD = hoursTD;
		this.hoursTP = hoursTP;
		this.hoursAMPHI = hoursAMPHI;
		this.hoursEXAM = hoursEXAM;
	}
	/**
	 * getter for the unit name
	 * @return the unit name
	 */
	public String getName() {
		return name;
	}
	/**
	 * setter for the unit name
	 * @param newName the new unit name
	 */
	public void setNom(String newName) {
		name = newName;
	}
	/**
	 * getter for the hourlyMass
	 * @return the hourly mass
	 */
	public int getHourlyMass() {
		return hourlyMass;
	}
	/**
	 * setter for the hourlyMass
	 * @param newHourlyMass the new hourly mass
	 */
	public void setMasseHoraire(int newHourlyMass) {
		hourlyMass = newHourlyMass;
	}
	/**
	 * getter for the TD hours
	 * @return the TD hours
	 */
	public int getHoursTD() {
		return hoursTD;
	}
	/**
	 * setter for the TD hours
	 * @param newHoursTD the new hours TD
	 */
	public void setHoursTD(int newHoursTD) {
		hoursTD = newHoursTD;
	}
	/**
	 * getter for the TP hours
	 * @return the TP hours
	 */
	public int getHoursTP() {
		return hoursTP;
	}
	/**
	 * setter for the TP hours
	 * @param newHoursTP the new hours TP
	 */
	public void setHeuresTP(int newHoursTP) {
		hoursTP = newHoursTP;
	}
	/**
	 * getter for the AMPHI hours
	 * @return the AMPHI hours
	 */
	public int getHoursAMPHI() {
		return hoursAMPHI;
	}
	/**
	 * setter for the AMPHI hours
	 * @param newHoursAMPHI the new hours AMPHI
	 */
	public void setHeuresAMPHI(int newHoursAMPHI) {
		hoursAMPHI = newHoursAMPHI;
	}
	/**
	 * getter for the EXAM hours
	 * @return the EXAM hours
	 */
	public int getHoursEXAM() {
		return hoursEXAM;
	}
	/**
	 * setter for the EXAM hours
	 * @param newHoursEXAM the new hours EXAM
	 */
	public void setHeuresEXAM(int newHoursEXAM) {
		hoursEXAM = newHoursEXAM;
	}
}
