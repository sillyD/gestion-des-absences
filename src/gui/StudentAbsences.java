package gui;

import java.awt.EventQueue;
import java.sql.*;

import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import dao.AbsenceDAO;
import dao.ConnectionDAO;
import dao.StudentDAO;
import net.proteanit.sql.DbUtils;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.JTextPane;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
/**
 * This is the frame used to allow a student to see his absences
 *  
 * @author DIOP-SONY
 * @version 1.0
 */
public class StudentAbsences extends ConnectionDAO{

	public JFrame frmShowAbsences;
	private JTable table;

	/**
	 * Create the application.
	 */
	public StudentAbsences() {
		super();
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmShowAbsences = new JFrame();
		frmShowAbsences.setTitle("SHOW ABSENCES");
		frmShowAbsences.setBounds(100, 100, 450, 300);
		frmShowAbsences.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmShowAbsences.setLocationRelativeTo(null);
		frmShowAbsences.getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 6, 414, 155);
		frmShowAbsences.getContentPane().add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		try {
			Connection con = DriverManager.getConnection(URL, LOGIN, PASS);
			PreparedStatement ps = con.prepareStatement("SELECT idSeance, idAbsence, typeAbsence, justificatif, etat FROM Absence WHERE idEtudiant = ?");
			if(Homepage.identifier.contains("esigelec")==false)
				ps.setInt(1, Integer.parseInt(Homepage.identifier));
			else
				ps.setInt(1, new StudentDAO().getId(Homepage.identifier));
			ResultSet rs = ps.executeQuery();
			table.setModel(DbUtils.resultSetToTableModel(rs));
			table.setEnabled(false);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frmShowAbsences.hide();
				new StudentChoice().frmStudentMenu.setVisible(true);
			}
		});
		btnCancel.setBackground(Color.LIGHT_GRAY);
		btnCancel.setForeground(Color.BLACK);
		btnCancel.setFont(new Font("Calibri", Font.BOLD, 15));
		btnCancel.setBounds(10, 227, 89, 23);
		frmShowAbsences.getContentPane().add(btnCancel);
		
		JButton btnAnticipatingAnAbsence = new JButton("Put a proof/Anticipating an absence");
		btnAnticipatingAnAbsence.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmShowAbsences.hide();
				new PutAProof_AnticipateAbsence().frmPutAProof.setVisible(true);
			}
		});
		btnAnticipatingAnAbsence.setForeground(Color.BLACK);
		btnAnticipatingAnAbsence.setFont(new Font("Calibri", Font.BOLD, 15));
		btnAnticipatingAnAbsence.setBackground(new Color(0, 191, 255));
		btnAnticipatingAnAbsence.setBounds(156, 227, 268, 23);
		frmShowAbsences.getContentPane().add(btnAnticipatingAnAbsence);
	}
}
