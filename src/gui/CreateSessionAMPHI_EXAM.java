package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JCheckBox;

import java.awt.Color;
import com.toedter.calendar.JDateChooser;

import dao.SessionDAO;
import dao.StudentsGroupDAO;
import dao.TeacherDAO;
import model.Session;

import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
/**
 * This is used to create an EXAM session or an AMPHI session
 *  
 * @author DIOP-SONY
 * @version 1.0
 */
public class CreateSessionAMPHI_EXAM {

	public JFrame frmCreateSessionamphiexam;
	private JTextField jtfSessionID;
	private JTextField jtfSchedule;
	private JTextField jtfDuration;
	private JTextField jtfUnit;
	private JTextField jtfAmphiRoom;
	private JTextField jtfNbreAbsences;

	/**
	 * Create the application.
	 */
	public CreateSessionAMPHI_EXAM() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCreateSessionamphiexam = new JFrame();
		frmCreateSessionamphiexam.setTitle("CREATE SESSION_AMPHI_EXAM");
		frmCreateSessionamphiexam.setBounds(100, 100, 588, 390);
		frmCreateSessionamphiexam.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmCreateSessionamphiexam.setLocationRelativeTo(null);
		frmCreateSessionamphiexam.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBounds(0, 0, 284, 355);
		frmCreateSessionamphiexam.getContentPane().add(panel);
		
		JPanel panel_1 = new JPanel();
		panel_1.setLayout(null);
		panel_1.setBounds(287, 0, 284, 351);
		frmCreateSessionamphiexam.getContentPane().add(panel_1);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(148, 143, 124, 73);
		panel_1.add(scrollPane);
		
		JPanel panel_2 = new JPanel();
		scrollPane.setViewportView(panel_2);
		panel_2.setBorder(UIManager.getBorder("ScrollPane.border"));
		panel_2.setLayout(new BoxLayout(panel_2, BoxLayout.Y_AXIS));
		
		int capacity = new StudentsGroupDAO().getUnitGroups().size();
		
		JCheckBox[] checkboxes = new JCheckBox[capacity];
		for(int i=0; i<checkboxes.length; i++) {
			checkboxes[i] = new JCheckBox(new StudentsGroupDAO().getUnitGroups().get(i));
			checkboxes[i].setFont(new Font("Calibri", Font.BOLD, 12));
		}
		for(int i=0; i<checkboxes.length; i++)
			panel_2.add(checkboxes[i]);
		
		JLabel label = new JLabel("Session'ID");
		label.setFont(new Font("Calibri", Font.BOLD, 16));
		label.setBounds(24, 34, 81, 14);
		panel.add(label);
		
		jtfSessionID = new JTextField("Exple: Programmer en JAVA_S15");
		jtfSessionID.setColumns(10);
		jtfSessionID.setBounds(112, 31, 162, 20);
		panel.add(jtfSessionID);
		
		JLabel label_1 = new JLabel("Date");
		label_1.setFont(new Font("Calibri", Font.BOLD, 16));
		label_1.setBounds(24, 87, 81, 14);
		panel.add(label_1);
		
		JLabel label_2 = new JLabel("Schedule");
		label_2.setFont(new Font("Calibri", Font.BOLD, 16));
		label_2.setBounds(24, 145, 81, 14);
		panel.add(label_2);
		
		JLabel lblDurationhour = new JLabel("Duration(Hour)");
		lblDurationhour.setFont(new Font("Calibri", Font.BOLD, 16));
		lblDurationhour.setBounds(10, 206, 117, 14);
		panel.add(lblDurationhour);
		
		JLabel label_4 = new JLabel("Type");
		label_4.setFont(new Font("Calibri", Font.BOLD, 16));
		label_4.setBounds(24, 262, 81, 14);
		panel.add(label_4);
		
		jtfSchedule = new JTextField();
		jtfSchedule.setColumns(10);
		jtfSchedule.setBounds(112, 142, 104, 20);
		panel.add(jtfSchedule);
		
		jtfDuration = new JTextField();
		jtfDuration.setColumns(10);
		jtfDuration.setBounds(148, 203, 104, 20);
		panel.add(jtfDuration);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"AMPHI", "EXAM"}));
		comboBox.setBounds(112, 259, 104, 20);
		panel.add(comboBox);
		
		JButton button = new JButton("Cancel");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmCreateSessionamphiexam.hide();
				new SessionChoice().frmSessionChoice.setVisible(true);
			}
		});
		button.setFont(new Font("Calibri", Font.BOLD, 13));
		button.setBackground(Color.LIGHT_GRAY);
		button.setBounds(24, 321, 77, 23);
		panel.add(button);
		
		JDateChooser dateChooser = new JDateChooser();
		dateChooser.setBounds(112, 87, 104, 20);
		panel.add(dateChooser);
		
		JLabel label_5 = new JLabel("Unit ");
		label_5.setFont(new Font("Calibri", Font.BOLD, 16));
		label_5.setBounds(24, 34, 81, 14);
		panel_1.add(label_5);
		
		JLabel label_6 = new JLabel("Teacher's ID");
		label_6.setFont(new Font("Calibri", Font.BOLD, 16));
		label_6.setBounds(24, 65, 92, 14);
		panel_1.add(label_6);
		
		JComboBox comboBox_1 = new JComboBox();
		ArrayList<Integer> teacherList = new TeacherDAO().getTeachersList();
		for(int i=0; i<teacherList.size(); i++)
			comboBox_1.addItem((Integer) teacherList.get(i));
		comboBox_1.setBounds(138, 62, 104, 20);
		panel_1.add(comboBox_1);
		
		JLabel label_7 = new JLabel("Replacing Teacher's ID");
		label_7.setFont(new Font("Calibri", Font.BOLD, 16));
		label_7.setBounds(17, 96, 160, 14);
		panel_1.add(label_7);
		
		JComboBox comboBox_2 = new JComboBox();
		for(int i=0; i<teacherList.size(); i++)
			comboBox_2.addItem((Integer) teacherList.get(i));
		comboBox_2.setBounds(170, 93, 104, 20);
		panel_1.add(comboBox_2);
		
		JLabel lblGroupsNumber = new JLabel("Groups Number");
		lblGroupsNumber.setFont(new Font("Calibri", Font.BOLD, 16));
		lblGroupsNumber.setBounds(10, 178, 118, 14);
		panel_1.add(lblGroupsNumber);
		
		JLabel lblAmphiroom = new JLabel("AmphiRoom");
		lblAmphiroom.setFont(new Font("Calibri", Font.BOLD, 16));
		lblAmphiroom.setBounds(24, 241, 104, 14);
		panel_1.add(lblAmphiroom);
		
		JLabel lblNbrOfAbsences = new JLabel("Nbr. of absences");
		lblNbrOfAbsences.setFont(new Font("Calibri", Font.BOLD, 16));
		lblNbrOfAbsences.setBounds(10, 272, 118, 14);
		panel_1.add(lblNbrOfAbsences);
		
		jtfUnit = new JTextField();
		jtfUnit.setColumns(10);
		jtfUnit.setBounds(138, 31, 104, 20);
		panel_1.add(jtfUnit);
		
		jtfAmphiRoom = new JTextField();
		jtfAmphiRoom.setColumns(10);
		jtfAmphiRoom.setBounds(138, 238, 104, 20);
		panel_1.add(jtfAmphiRoom);
		
		jtfNbreAbsences = new JTextField("0");
		jtfNbreAbsences.setColumns(10);
		jtfNbreAbsences.setBounds(138, 269, 104, 20);
		panel_1.add(jtfNbreAbsences);
		
		JButton button_1 = new JButton("add");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
				String date = sdf.format(dateChooser.getDate());
				String groups = null;
				for(int i=0; i<checkboxes.length; i++) {
					if (checkboxes[i].isSelected()==true) {
						groups += checkboxes[i].getText()+"/";
					}
				}
				if(new SessionDAO().add(new Session(jtfSessionID.getText(), date, jtfSchedule.getText(), Integer.parseInt(jtfDuration.getText()), (String) comboBox.getSelectedItem(), jtfUnit.getText(), (int) comboBox_1.getSelectedItem(), null, jtfAmphiRoom.getText(), Integer.parseInt(jtfNbreAbsences.getText()), "undone"), groups.replace("null", ""), (int) comboBox_2.getSelectedItem())==0)
					JOptionPane.showMessageDialog(frmCreateSessionamphiexam, "Database Error !!!");
				else
					JOptionPane.showMessageDialog(frmCreateSessionamphiexam, "Successfully !!!");
			}
		});
		button_1.setFont(new Font("Calibri", Font.BOLD, 13));
		button_1.setBackground(new Color(135, 206, 250));
		button_1.setBounds(10, 321, 77, 23);
		panel_1.add(button_1);
		
		JButton button_2 = new JButton("update");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
				String date = sdf.format(dateChooser.getDate());
				String groups = null;
				for(int i=0; i<checkboxes.length; i++) {
					if (checkboxes[i].isSelected()==true) 
						groups += checkboxes[i].getText()+"/";
				}
				if(new SessionDAO().update(new Session(jtfSessionID.getText(), date, jtfSchedule.getText(), Integer.parseInt(jtfDuration.getText()), (String) comboBox.getSelectedItem(), jtfUnit.getText(), (int) comboBox_1.getSelectedItem(), null, jtfAmphiRoom.getText(), Integer.parseInt(jtfNbreAbsences.getText()), "undone"), groups.replace("null", ""), (int) comboBox_2.getSelectedItem())==0)
					JOptionPane.showMessageDialog(frmCreateSessionamphiexam, "Database Error !!!\\n Verify if the session ID exists");
				else
					JOptionPane.showMessageDialog(frmCreateSessionamphiexam, "Successfully !!!");
			}
		});
		button_2.setFont(new Font("Calibri", Font.BOLD, 13));
		button_2.setBackground(new Color(255, 218, 185));
		button_2.setBounds(97, 321, 80, 23);
		panel_1.add(button_2);
		
		JButton button_3 = new JButton("delete");
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
				String date = sdf.format(dateChooser.getDate());
				if(new SessionDAO().delete(new Session(jtfSessionID.getText(), date, jtfSchedule.getText(), Integer.parseInt(jtfDuration.getText()), (String) comboBox.getSelectedItem(), jtfUnit.getText(), (int) comboBox_1.getSelectedItem(), null, jtfAmphiRoom.getText(), Integer.parseInt(jtfNbreAbsences.getText()), "undone"))==0)
					JOptionPane.showMessageDialog(frmCreateSessionamphiexam, "Database Error !!!\n Verify if the session ID exists");
				else
					JOptionPane.showMessageDialog(frmCreateSessionamphiexam, "Successfully !!!");
			}
		});
		button_3.setFont(new Font("Calibri", Font.BOLD, 13));
		button_3.setBackground(new Color(255, 51, 0));
		button_3.setBounds(187, 321, 77, 23);
		panel_1.add(button_3);
		
		JLabel label_8 = new JLabel("Put the same teacher'ID if there's no replacing");
		label_8.setFont(new Font("Tahoma", Font.PLAIN, 10));
		label_8.setForeground(Color.RED);
		label_8.setBounds(18, 121, 254, 14);
		panel_1.add(label_8);
	}
}
