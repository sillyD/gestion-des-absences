package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.GridLayout;
import javax.swing.JPanel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.AbstractListModel;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JFormattedTextField;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileSystemView;

import java.awt.Color;
import java.awt.Desktop;

import javax.swing.JSplitPane;
import javax.swing.JProgressBar;
import javax.swing.JScrollBar;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBoxMenuItem;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.sql.SQLIntegrityConstraintViolationException;

import javax.swing.JTree;
import javax.swing.tree.DefaultTreeModel;

import dao.StudentsGroupDAO;
import model.StudentsGroup;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.JTabbedPane;
/**
 * This is used to manage the students groups
 *  
 * @author DIOP-SONY
 * @version 1.0
 */
public class GroupManaging {

	public JFrame frmGestionDeGroupes;
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Create the application.
	 */
	public GroupManaging() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmGestionDeGroupes = new JFrame();
		frmGestionDeGroupes.setTitle("Student Groups Management");
		frmGestionDeGroupes.setBounds(100, 100, 450, 300);
		frmGestionDeGroupes.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmGestionDeGroupes.setLocationRelativeTo(null);
		frmGestionDeGroupes.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Number");
		lblNewLabel.setFont(new Font("Calibri", Font.BOLD, 15));
		lblNewLabel.setBounds(32, 39, 62, 14);
		frmGestionDeGroupes.getContentPane().add(lblNewLabel);
		
		JLabel lblTypeDeGroupe = new JLabel("Type");
		lblTypeDeGroupe.setFont(new Font("Calibri", Font.BOLD, 15));
		lblTypeDeGroupe.setBounds(32, 75, 62, 14);
		frmGestionDeGroupes.getContentPane().add(lblTypeDeGroupe);
		
		textField = new JTextField();
		textField.setBounds(115, 36, 86, 20);
		frmGestionDeGroupes.getContentPane().add(textField);
		textField.setColumns(10);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"unit", "english", "exam"}));
		comboBox.setBounds(104, 72, 97, 20);
		frmGestionDeGroupes.getContentPane().add(comboBox);
		
		JButton btnAnnuler = new JButton("Cancel");
		btnAnnuler.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmGestionDeGroupes.hide();
				new ManagerChoice().frmMenuDuGestionnaire.setVisible(true);
			}
		});
		btnAnnuler.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnAnnuler.setBackground(new Color(192, 192, 192));
		btnAnnuler.setBounds(318, 227, 89, 23);
		frmGestionDeGroupes.getContentPane().add(btnAnnuler);
		
		JButton btnSuivant = new JButton("Add");
		btnSuivant.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// we verify if the user respect the naming group model
				if(textField.getText().contains("UN-")==true || textField.getText().contains("EN-")==true || textField.getText().contains("EX-")==true) {
					if(new StudentsGroupDAO().add(new StudentsGroup(textField.getText(), (String) comboBox.getSelectedItem(), textField_1.getText()))!=0)
						JOptionPane.showMessageDialog(frmGestionDeGroupes, "Successfully !");
					else
						JOptionPane.showMessageDialog(frmGestionDeGroupes, "Database Error !");
				}
				else
					JOptionPane.showMessageDialog(frmGestionDeGroupes, "Respect the naming model group\n exple: UN-1 --> Unit group 1");
				textField.setText("");
				textField_1.setText("");
			}
		});
		btnSuivant.setBackground(new Color(60, 179, 113));
		btnSuivant.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnSuivant.setBounds(272, 193, 89, 23);
		frmGestionDeGroupes.getContentPane().add(btnSuivant);
		frmGestionDeGroupes.getRootPane().setDefaultButton(btnSuivant);
		
		JLabel lblNewLabel_1 = new JLabel("Ex: UN-1 -> Unit Group 1");
		lblNewLabel_1.setBounds(211, 24, 213, 14);
		frmGestionDeGroupes.getContentPane().add(lblNewLabel_1);
		
		JLabel lblExExam = new JLabel(" EX-3 -> Exam Group 3");
		lblExExam.setBounds(221, 39, 126, 14);
		frmGestionDeGroupes.getContentPane().add(lblExExam);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(152, 112, 133, 20);
		frmGestionDeGroupes.getContentPane().add(textField_1);
		
		JButton btnPutPlanning = new JButton("Put Planning");
		btnPutPlanning.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// allows the selection of a document in the system
				JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
				jfc.setApproveButtonText("cliquez ici");
				
				int returnValue = jfc.showOpenDialog(null);
				if (returnValue == JFileChooser.APPROVE_OPTION) {
					File selectedFile = jfc.getSelectedFile();
				    textField_1.setText(selectedFile.getAbsolutePath());
				}
			}
		});
		btnPutPlanning.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnPutPlanning.setBackground(new Color(30, 144, 255));
		btnPutPlanning.setBounds(10, 111, 111, 23);
		frmGestionDeGroupes.getContentPane().add(btnPutPlanning);
		
		JButton btnModifier = new JButton("Update");
		btnModifier.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(textField.getText().contains("UN-")==true || textField.getText().contains("EN-")==true || textField.getText().contains("EX-")==true) {
					if(new StudentsGroupDAO().update(new StudentsGroup(textField.getText(), (String) comboBox.getSelectedItem(), textField_1.getText()))!=0)
						JOptionPane.showMessageDialog(frmGestionDeGroupes, "Successfully !");
					else
						JOptionPane.showMessageDialog(frmGestionDeGroupes, "Database Error !");
				}
				else
					JOptionPane.showMessageDialog(frmGestionDeGroupes, "Respect the naming model group\n exple: UN-1 --> Unit group 1");
				textField.setText("");
				textField_1.setText("");
			}
		});
		btnModifier.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnModifier.setBackground(new Color(154, 205, 50));
		btnModifier.setBounds(166, 193, 89, 23);
		frmGestionDeGroupes.getContentPane().add(btnModifier);
		
		JButton btnSupprimer = new JButton("Delete");
		btnSupprimer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(textField.getText().contains("UN-")==true || textField.getText().contains("EN-")==true || textField.getText().contains("EX-")==true) {
					if(new StudentsGroupDAO().delete(new StudentsGroup(textField.getText(), (String) comboBox.getSelectedItem(), textField_1.getText()))!=0)
						JOptionPane.showMessageDialog(frmGestionDeGroupes, "Successfully !");
					else
						JOptionPane.showMessageDialog(frmGestionDeGroupes, "Database Error !");
				}
				else 
					JOptionPane.showMessageDialog(frmGestionDeGroupes, "Respect the naming model group\n exple: UN-1 --> Unit group 1");
				textField.setText("");
				textField_1.setText("");
			}
		});
		btnSupprimer.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnSupprimer.setBackground(new Color(220, 20, 60));
		btnSupprimer.setBounds(60, 193, 89, 23);
		frmGestionDeGroupes.getContentPane().add(btnSupprimer);
		
		JButton btnPutStudentsIn = new JButton("Put students in a group");
		btnPutStudentsIn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frmGestionDeGroupes.hide();
				new AffectStudent1().frmAffectStudentsIn.setVisible(true);
			}
		});
		btnPutStudentsIn.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnPutStudentsIn.setBackground(new Color(221, 160, 221));
		btnPutStudentsIn.setBounds(70, 227, 238, 23);
		frmGestionDeGroupes.getContentPane().add(btnPutStudentsIn);
		
		JLabel lblInPdfFormat = new JLabel("in PDF format");
		lblInPdfFormat.setBounds(295, 115, 129, 14);
		frmGestionDeGroupes.getContentPane().add(lblInPdfFormat);
		
		
	}
}
