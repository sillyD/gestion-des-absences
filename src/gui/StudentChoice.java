package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import dao.StudentDAO;

import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.awt.event.ActionEvent;
/**
 * This is the frame used to show the choices of a student
 *  
 * @author DIOP-SONY
 * @version 1.0
 */
public class StudentChoice {

	public JFrame frmStudentMenu;

	/**
	 * Create the application.
	 */
	public StudentChoice() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmStudentMenu = new JFrame();
		frmStudentMenu.setTitle("STUDENT MENU");
		frmStudentMenu.setBounds(100, 100, 450, 300);
		frmStudentMenu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmStudentMenu.setLocationRelativeTo(null);
		frmStudentMenu.getContentPane().setLayout(null);
		
		JLabel lblWelcomeToThe = new JLabel("WELCOME TO THE STUDENT MENU");
		lblWelcomeToThe.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 15));
		lblWelcomeToThe.setBounds(88, 38, 241, 14);
		frmStudentMenu.getContentPane().add(lblWelcomeToThe);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Show Planning", "Show absences", "Show notifications"}));
		comboBox.setBounds(144, 109, 260, 20);
		frmStudentMenu.getContentPane().add(comboBox);
		
		JButton button = new JButton("OK");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				switch ((String) comboBox.getSelectedItem()) {
					
					case "Show Planning": 
						String planning = new StudentDAO().getPlanning(Homepage.identifier);
						// we verify if a planning is available
						if (planning == null)
							JOptionPane.showMessageDialog(null, "Aucun planning disponible");
						else {
							File pdfFile = new File(planning);
							if (Desktop.isDesktopSupported())
							{
								Desktop desktop = Desktop.getDesktop();
								try {
									desktop.open(pdfFile);
		
								} catch (IOException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
							}
						}
						break;
						
					case "Show absences":
						frmStudentMenu.hide();
						new StudentAbsences().frmShowAbsences.setVisible(true);
						break;
						
					case "Show notifications":
						frmStudentMenu.hide();
						new Notifications().frmNotifications.setVisible(true);
						break;
				}
			}
		});
		button.setBackground(new Color(50, 205, 50));
		button.setBounds(62, 203, 89, 23);
		frmStudentMenu.getContentPane().add(button);
		
		JButton btnLogOut = new JButton("Log out\r\n");
		btnLogOut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmStudentMenu.hide();
				new Homepage().frmAcceuil.setVisible(true);
			}
		});
		btnLogOut.setBackground(new Color(250, 128, 114));
		btnLogOut.setBounds(268, 203, 136, 23);
		frmStudentMenu.getContentPane().add(btnLogOut);
	}
}
