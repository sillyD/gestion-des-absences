package gui;

import java.awt.*;
import java.util.*;

import javax.swing.*;

import dao.AbsenceDAO;
import dao.SessionDAO;
import dao.StudentDAO;
import dao.StudentsGroupDAO;
import model.Absence;

import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.awt.event.ActionEvent;
import com.jgoodies.forms.factories.DefaultComponentFactory;
import com.toedter.calendar.JDateChooser;
/**
 * This is the second frame to make the call
 *  
 * @author DIOP-SONY
 * @version 1.0
 */
public class MakeTheCall2 {

	public JFrame frmMakeTheCall;
	public JPanel checkBoxPanel;
	//public ArrayList<JCheckBox> checkboxes;
	public JCheckBox check1;
	
	/**
	 * Create the application.
	 */
	public MakeTheCall2() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmMakeTheCall = new JFrame();
		frmMakeTheCall.setTitle("MAKE THE CALL_2");
		frmMakeTheCall.setBounds(100, 100, 800, 586);
		frmMakeTheCall.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMakeTheCall.setLocationRelativeTo(null);
		frmMakeTheCall.getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 40, 343, 500);
		frmMakeTheCall.getContentPane().add(scrollPane);
		
		JPanel panel = new JPanel();
		panel.setBorder(UIManager.getBorder("ScrollPane.border"));
		scrollPane.setViewportView(panel);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		int capacity = new StudentsGroupDAO().getGroupStaffing(MakeTheCall1.groupNumber);
		String[] names = new StudentsGroupDAO().getStudentList(MakeTheCall1.groupNumber);
		String[] nameSplit;
		
		JCheckBox[] checkboxes = new JCheckBox[capacity];
		for(int i=0; i<checkboxes.length; i++) {
			nameSplit = names[i].split("/");
			checkboxes[i] = new JCheckBox(nameSplit[0]+" "+nameSplit[1]);
			checkboxes[i].setFont(new Font("Calibri", Font.BOLD, 12));
		}
		
		for(int i=0; i<checkboxes.length; i++)
			panel.add(checkboxes[i]);
		
		JLabel lblStudentList = new JLabel("STUDENTS LIST OF GROUP : "+MakeTheCall1.groupNumber);
		lblStudentList.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 20));
		lblStudentList.setBounds(37, 11, 299, 18);
		frmMakeTheCall.getContentPane().add(lblStudentList);
		
		JSeparator separator = new JSeparator();
		separator.setOrientation(SwingConstants.VERTICAL);
		separator.setBounds(363, 11, 14, 532);
		frmMakeTheCall.getContentPane().add(separator);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmMakeTheCall.hide();
				new MakeTheCall1().frmMakeTheCall1.setVisible(true);
			}
		});
		btnCancel.setFont(new Font("Cambria", Font.BOLD, 15));
		btnCancel.setBackground(new Color(192, 192, 192));
		btnCancel.setBounds(644, 511, 114, 23);
		frmMakeTheCall.getContentPane().add(btnCancel);
		
		JLabel lblIdDeLa = new JLabel("ID de la s\u00E9ance");
		lblIdDeLa.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblIdDeLa.setBounds(401, 238, 139, 14);
		frmMakeTheCall.getContentPane().add(lblIdDeLa);	
		
		JComboBox comboBox = new JComboBox();
		ArrayList<String> sessionlist = new SessionDAO().getSessions();
		for(int i=0; i<sessionlist.size(); i++)
			comboBox.addItem(sessionlist.get(i));
		comboBox.setBounds(550, 237, 139, 20);
		frmMakeTheCall.getContentPane().add(comboBox);
		
		JButton btnFinishTheCall = new JButton("Finish the call\r\n");
		btnFinishTheCall.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int error_count = 0; // counter for the number of error when we add absences 
				int success_count = 0; // counter for the number of success when we add absences 
				int absences_count = 0;
				String[] getID;
				for(int i=0; i<checkboxes.length; i++) {
					if(checkboxes[i].isSelected()==true) {
						absences_count++;
						getID = names[i].split("/");
						SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
						if(new AbsenceDAO().add("ordinaire", Integer.parseInt(getID[2]), (String) comboBox.getSelectedItem())==0)
							error_count++;
						else
							success_count++;
						
						// we add the number of absence hours for the missing student
						new StudentDAO().updateNbrAbsencesHours(new SessionDAO().getDuration((String)comboBox.getSelectedItem()), Integer.parseInt(getID[2]), "unexcused");
					}
				}
				// we add the number of absences
				if(new SessionDAO().updateNbreAbsences(absences_count, (String)comboBox.getSelectedItem())!=0)
					JOptionPane.showMessageDialog(frmMakeTheCall, "session's database updated !!!");
				else
					JOptionPane.showMessageDialog(frmMakeTheCall, "Database Error !!!");
				
				if(error_count==0)
					JOptionPane.showMessageDialog(frmMakeTheCall, "Successfully !!!");
				else
					JOptionPane.showMessageDialog(frmMakeTheCall, "There is "+error_count+" errors and "+success_count+" success during the absences adding");
				frmMakeTheCall.hide();
				new TeacherChoice().frmTeacherMenu.setVisible(true);
			}
		});
		btnFinishTheCall.setFont(new Font("Cambria", Font.BOLD, 15));
		btnFinishTheCall.setBackground(new Color(100, 149, 237));
		btnFinishTheCall.setBounds(459, 511, 159, 23);
		frmMakeTheCall.getContentPane().add(btnFinishTheCall);
		frmMakeTheCall.getRootPane().setDefaultButton(btnFinishTheCall);
	}
}
