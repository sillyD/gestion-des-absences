package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import dao.AbsenceDAO;
import dao.NoteDAO;
import dao.NotificationDAO;
import dao.QuotaDAO;
import dao.SessionDAO;
import dao.StudentDAO;

import java.awt.Font;
import java.util.ArrayList;
import java.util.logging.ErrorManager;

import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.awt.event.ActionEvent;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
/**
 * This is used to manage the absences of the students
 *  
 * @author DIOP-SONY
 * @version 1.0
 */
public class ManagementAbsences {

	public JFrame frmManagementAbsences;

	/**
	 * Create the application.
	 */
	public ManagementAbsences() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmManagementAbsences = new JFrame();
		frmManagementAbsences.setTitle("MANAGEMENT ABSENCES");
		frmManagementAbsences.setBounds(100, 100, 551, 353);
		frmManagementAbsences.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmManagementAbsences.getContentPane().setLayout(null);
		
		JLabel lblStudentsWhoMissed = new JLabel("Students who missed an exam(justified)");
		lblStudentsWhoMissed.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 13));
		lblStudentsWhoMissed.setBounds(10, 11, 214, 41);
		frmManagementAbsences.getContentPane().add(lblStudentsWhoMissed);
		
		JComboBox studentBox1 = new JComboBox();
		ArrayList<Integer> studentlistunex = new AbsenceDAO().getStudentListUnexcusedAndJustified();
		for(int i=0; i<studentlistunex.size(); i++)
			studentBox1.addItem((int)studentlistunex.get(i));
		studentBox1.setBounds(20, 39, 94, 20);
		frmManagementAbsences.getContentPane().add(studentBox1);
		
		JComboBox examSessionBox = new JComboBox();
		ArrayList<String> examSessionList = new SessionDAO().getExamSessions();
		for(int i=0; i<examSessionList.size(); i++)
			examSessionBox.addItem(examSessionList.get(i));
		examSessionBox.setBounds(20, 64, 94, 20);
		frmManagementAbsences.getContentPane().add(examSessionBox);
		
		JButton btnShowProof = new JButton("Show proof");
		btnShowProof.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String proof = new AbsenceDAO().getProof((int)studentBox1.getSelectedItem(), (String) examSessionBox.getSelectedItem());
				// we verify if a planning is available
				if (proof == null)
					JOptionPane.showMessageDialog(null, "There is no proof");
				else {
					File pdfFile = new File(proof);
					if (Desktop.isDesktopSupported())
					{
						Desktop desktop = Desktop.getDesktop();
						try {
							desktop.open(pdfFile);

						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				}
			}
		});
		btnShowProof.setBackground(new Color(102, 205, 170));
		btnShowProof.setFont(new Font("Calibri", Font.BOLD, 13));
		btnShowProof.setBounds(135, 63, 100, 23);
		frmManagementAbsences.getContentPane().add(btnShowProof);
		
		JButton btnValidate = new JButton("Validate");
		btnValidate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(new AbsenceDAO().excuseAbsence((String)examSessionBox.getSelectedItem(), (int)studentBox1.getSelectedItem())!=0) {
					if(new StudentDAO().updateNbrAbsencesHours(new SessionDAO().getDuration((String) examSessionBox.getSelectedItem()), (int)studentBox1.getSelectedItem(), "excusee")!=0)
						JOptionPane.showMessageDialog(frmManagementAbsences, "Successfully !!!");
					else
						JOptionPane.showMessageDialog(frmManagementAbsences, "Database Error!!!");
				}
				else
					JOptionPane.showMessageDialog(frmManagementAbsences, "Database Error!!!");
				
				// we send a message to the student
				if(new NotificationDAO().sendMessage((int)studentBox1.getSelectedItem(), "You have a catch-up for the session "+(String)examSessionBox.getSelectedItem())!=0)
					JOptionPane.showMessageDialog(frmManagementAbsences, "Message sent to the student uccessfully !!!");
				else
					JOptionPane.showMessageDialog(frmManagementAbsences, "Database Error!!!");
			}
		});
		btnValidate.setFont(new Font("Calibri", Font.BOLD, 13));
		btnValidate.setBackground(new Color(135, 206, 250));
		btnValidate.setBounds(71, 107, 100, 23);
		frmManagementAbsences.getContentPane().add(btnValidate);
		
		JLabel lblClickOnvalidate = new JLabel("Click on 'Validate' to excuse the absence");
		lblClickOnvalidate.setFont(new Font("Tahoma", Font.PLAIN, 9));
		lblClickOnvalidate.setForeground(new Color(255, 0, 0));
		lblClickOnvalidate.setBounds(10, 94, 225, 14);
		frmManagementAbsences.getContentPane().add(lblClickOnvalidate);
		
		JButton btnGiveTo = new JButton("Give 0 to the unjustified absences in exam");
		btnGiveTo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ArrayList<String> studentList = new AbsenceDAO().getStudentListUnexcusedAndUnjustified();
				String[] list;
				int error_count = 0;
				int success_count = 0;
				for(int i=0; i<studentList.size(); i++) {
					list = studentList.get(i).split("/");
					if(new NoteDAO().giveZero(Integer.parseInt(list[0]), list[1], 0)!=0)
						success_count++;
					else
						error_count++;
				}
				if(error_count==0)
					JOptionPane.showMessageDialog(frmManagementAbsences, "Successfully !!!");
				else
					JOptionPane.showMessageDialog(frmManagementAbsences, "There is "+error_count+" students who haven't been added !!!\n they were in the database problably");
			}
		});
		btnGiveTo.setFont(new Font("Calibri", Font.BOLD, 13));
		btnGiveTo.setBackground(new Color(244, 164, 96));
		btnGiveTo.setBounds(245, 63, 286, 23);
		frmManagementAbsences.getContentPane().add(btnGiveTo);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frmManagementAbsences.hide();
				new ManagerChoice().frmMenuDuGestionnaire.setVisible(true);
			}
		});
		btnCancel.setFont(new Font("Calibri", Font.BOLD, 13));
		btnCancel.setBackground(new Color(192, 192, 192));
		btnCancel.setBounds(14, 280, 100, 23);
		frmManagementAbsences.getContentPane().add(btnCancel);
		
		JSeparator separator = new JSeparator();
		separator.setOrientation(SwingConstants.VERTICAL);
		separator.setBounds(239, 0, 51, 314);
		frmManagementAbsences.getContentPane().add(separator);
		
		JButton btnNotifyStudentsFor = new JButton("Notify students for penalties");
		btnNotifyStudentsFor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ArrayList<String> students = new StudentDAO().getStudentListWithAbsenceHours();
				String[] list;
				int error_count = 0;
				int success_count = 0;
				for(int i=0; i<students.size(); i++) {
					list = students.get(i).split("/");
					if(new QuotaDAO().verifyFirstQuota(Integer.parseInt(list[1])) > 0) {
						if(new QuotaDAO().verifySecondQuota(Integer.parseInt(list[1])) > 0) {
							if(new QuotaDAO().verifyThirdQuota(Integer.parseInt(list[1])) > 0) {
								if(new QuotaDAO().verifyFourthQuota(Integer.parseInt(list[1])) > 0) {
									if(new QuotaDAO().verifyFifthQuota(Integer.parseInt(list[1])) > 0) {
										if(new QuotaDAO().verifySixthQuota(Integer.parseInt(list[1])) > 0) {
											// we send a message to the student
											if(new NotificationDAO().sendMessage(Integer.parseInt(list[0]), "You have exceeded the sixth quota ")!=0)
												success_count++;
											else
												error_count++;
										}
										else {
											// we send a message to the student
											if(new NotificationDAO().sendMessage(Integer.parseInt(list[0]), "You have exceeded the fifth quota ")!=0)
												success_count++;
											else
												error_count++;
										}
									}
									else {
										// we send a message to the student
										if(new NotificationDAO().sendMessage(Integer.parseInt(list[0]), "You have exceeded the fourth quota ")!=0)
											success_count++;
										else
											error_count++;
									}
								}
								else {
									// we send a message to the student
									if(new NotificationDAO().sendMessage(Integer.parseInt(list[0]), "You have exceeded the third quota ")!=0)
										success_count++;
									else
										error_count++;
								}
							}
							else {
								// we send a message to the student
								if(new NotificationDAO().sendMessage(Integer.parseInt(list[0]), "You have exceeded the second quota ")!=0)
									success_count++;
								else
									error_count++;
							}
						}
						else {
							// we send a message to the student
							if(new NotificationDAO().sendMessage(Integer.parseInt(list[0]), "You have exceeded the first quota ")!=0)
								success_count++;
							else
								error_count++;
						}
					}
				}
				if(error_count==0)
					JOptionPane.showMessageDialog(frmManagementAbsences, "Messages sent uccessfully !!!");
				else
					JOptionPane.showMessageDialog(frmManagementAbsences, "Database error(s)");
			}
		});
		btnNotifyStudentsFor.setFont(new Font("Calibri", Font.BOLD, 13));
		btnNotifyStudentsFor.setBackground(new Color(238, 130, 238));
		btnNotifyStudentsFor.setBounds(274, 194, 231, 23);
		frmManagementAbsences.getContentPane().add(btnNotifyStudentsFor);
	}
}
