package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import dao.TeacherDAO;

import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Desktop;

import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.awt.event.ActionEvent;
/**
 * This is the frame used to show the choices of a teacher
 *  
 * @author DIOP-SONY
 * @version 1.0
 */
public class TeacherChoice {

	public JFrame frmTeacherMenu;

	/**
	 * Create the application.
	 */
	public TeacherChoice() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmTeacherMenu = new JFrame();
		frmTeacherMenu.setTitle("TEACHER MENU");
		frmTeacherMenu.setBounds(100, 100, 450, 300);
		frmTeacherMenu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmTeacherMenu.setLocationRelativeTo(null);
		frmTeacherMenu.getContentPane().setLayout(null);
		
		JLabel lblWelcomeToThe = new JLabel("WELCOME TO THE TEACHER MENU");
		lblWelcomeToThe.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 15));
		lblWelcomeToThe.setBounds(86, 41, 241, 14);
		frmTeacherMenu.getContentPane().add(lblWelcomeToThe);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Show planning", "Make the call", "Show absences for a student"}));
		comboBox.setBounds(138, 103, 260, 20);
		frmTeacherMenu.getContentPane().add(comboBox);
		
		JButton button = new JButton("OK");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				switch((String) comboBox.getSelectedItem()) {
				
				case "Show planning":
					String planning = new TeacherDAO().getPlanning(Homepage.identifier);
					// we verify if the planning exits
					if(planning == null)
						JOptionPane.showMessageDialog(frmTeacherMenu, "There is no planning for this teacher !!!");
					else {
						File pdfFile = new File(planning);
						if(Desktop.isDesktopSupported()) {
							Desktop desktop = Desktop.getDesktop();
							try {
								desktop.open(pdfFile);
							} catch (IOException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}
					}
					break;
					
				case "Make the call":
					frmTeacherMenu.hide();
					new MakeTheCall1().frmMakeTheCall1.setVisible(true);
					break;
					
				case "Show absences for a student":
					frmTeacherMenu.hide();
					new ShowStudentAbsences().frmShowStudentAbsences.setVisible(true);
					break;
				}
			}
		});
		button.setBackground(new Color(50, 205, 50));
		button.setBounds(69, 205, 89, 23);
		frmTeacherMenu.getContentPane().add(button);
		
		JButton button_1 = new JButton("Log out\r\n");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmTeacherMenu.hide();
				new Homepage().frmAcceuil.setVisible(true);
			}
		});
		button_1.setBackground(new Color(250, 128, 114));
		button_1.setBounds(262, 205, 136, 23);
		frmTeacherMenu.getContentPane().add(button_1);
	}

}
