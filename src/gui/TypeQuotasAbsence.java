package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JTabbedPane;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;

import java.awt.GridLayout;
import javax.swing.JFormattedTextField;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;

import dao.TypeQuotasAbsencesDAO;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
/**
 * This is the frame allowing the manager to create the quotas and absences types
 *  
 * @author DIOP-SONY
 * @version 1.0
 */
public class TypeQuotasAbsence {

	public JFrame frmDefineAbsenceTypes;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;

	/**
	 * Create the application.
	 */
	public TypeQuotasAbsence() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmDefineAbsenceTypes = new JFrame();
		frmDefineAbsenceTypes.setTitle("Define Absence types and quotas");
		frmDefineAbsenceTypes.setBounds(100, 100, 583, 319);
		frmDefineAbsenceTypes.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmDefineAbsenceTypes.setLocationRelativeTo(null);
		frmDefineAbsenceTypes.getContentPane().setLayout(null);
		
		JLabel lblDefineAbsenceTypes = new JLabel("Create absence types");
		lblDefineAbsenceTypes.setBounds(365, 28, 146, 14);
		frmDefineAbsenceTypes.getContentPane().add(lblDefineAbsenceTypes);
		lblDefineAbsenceTypes.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 15));
		
		textField = new JTextField();
		textField.setBounds(333, 76, 188, 20);
		frmDefineAbsenceTypes.getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btnAddTypes = new JButton("Add");
		btnAddTypes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(new TypeQuotasAbsencesDAO().addAbsenceType(textField.getText())==1)
					JOptionPane.showMessageDialog(frmDefineAbsenceTypes, "Successfully");
				else
					JOptionPane.showMessageDialog(frmDefineAbsenceTypes, "Database error!!!");
				textField.setText("");
			}
		});
		btnAddTypes.setBounds(385, 128, 89, 23);
		frmDefineAbsenceTypes.getContentPane().add(btnAddTypes);
		btnAddTypes.setBackground(new Color(30, 144, 255));
		btnAddTypes.setFont(new Font("Calibri", Font.BOLD, 13));
		
		JButton btnCancel = new JButton("Cancel\r\n");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmDefineAbsenceTypes.hide();
				new ManagerChoice().frmMenuDuGestionnaire.setVisible(true);
			}
		});
		btnCancel.setFont(new Font("Calibri", Font.BOLD, 13));
		btnCancel.setBackground(new Color(192, 192, 192));
		btnCancel.setBounds(446, 246, 89, 23);
		frmDefineAbsenceTypes.getContentPane().add(btnCancel);
		
		JLabel label = new JLabel("Define absence quotas");
		label.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 15));
		label.setBounds(63, 11, 146, 14);
		frmDefineAbsenceTypes.getContentPane().add(label);
		
		JSeparator separator = new JSeparator();
		separator.setOrientation(SwingConstants.VERTICAL);
		separator.setBounds(281, 0, 30, 280);
		frmDefineAbsenceTypes.getContentPane().add(separator);
		
		JLabel lblSeuil = new JLabel("Threshold 1");
		lblSeuil.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 15));
		lblSeuil.setBounds(23, 66, 79, 14);
		frmDefineAbsenceTypes.getContentPane().add(lblSeuil);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(112, 63, 97, 20);
		frmDefineAbsenceTypes.getContentPane().add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(112, 88, 97, 20);
		frmDefineAbsenceTypes.getContentPane().add(textField_2);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(112, 113, 97, 20);
		frmDefineAbsenceTypes.getContentPane().add(textField_3);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(112, 138, 97, 20);
		frmDefineAbsenceTypes.getContentPane().add(textField_4);
		
		textField_5 = new JTextField();
		textField_5.setColumns(10);
		textField_5.setBounds(112, 163, 97, 20);
		frmDefineAbsenceTypes.getContentPane().add(textField_5);
		
		textField_6 = new JTextField();
		textField_6.setColumns(10);
		textField_6.setBounds(112, 188, 97, 20);
		frmDefineAbsenceTypes.getContentPane().add(textField_6);
		
		JButton buttonQuotas = new JButton("Update");
		buttonQuotas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(new TypeQuotasAbsencesDAO().addAbsenceQuotas(Integer.parseInt(textField_1.getText()), Integer.parseInt(textField_2.getText()), Integer.parseInt(textField_3.getText()), Integer.parseInt(textField_4.getText()), Integer.parseInt(textField_5.getText()), Integer.parseInt(textField_6.getText()))==1)
					JOptionPane.showMessageDialog(frmDefineAbsenceTypes, "Successfully");
				else
					JOptionPane.showMessageDialog(frmDefineAbsenceTypes, "Database error!!!");
				textField_1.setText("");
				textField_2.setText("");
				textField_3.setText("");
				textField_4.setText("");
				textField_5.setText("");
				textField_6.setText("");
			}
		});
		buttonQuotas.setFont(new Font("Calibri", Font.BOLD, 13));
		buttonQuotas.setBackground(new Color(30, 144, 255));
		buttonQuotas.setBounds(92, 228, 89, 23);
		frmDefineAbsenceTypes.getContentPane().add(buttonQuotas);
		
		JLabel lblThreshold = new JLabel("Threshold 2");
		lblThreshold.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 15));
		lblThreshold.setBounds(23, 91, 79, 14);
		frmDefineAbsenceTypes.getContentPane().add(lblThreshold);
		
		JLabel lblThreshold_1 = new JLabel("Threshold 3");
		lblThreshold_1.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 15));
		lblThreshold_1.setBounds(23, 116, 79, 14);
		frmDefineAbsenceTypes.getContentPane().add(lblThreshold_1);
		
		JLabel lblThreshold_2 = new JLabel("Threshold 4");
		lblThreshold_2.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 15));
		lblThreshold_2.setBounds(23, 141, 79, 14);
		frmDefineAbsenceTypes.getContentPane().add(lblThreshold_2);
		
		JLabel lblThreshold_3 = new JLabel("Threshold 5");
		lblThreshold_3.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 15));
		lblThreshold_3.setBounds(23, 166, 79, 14);
		frmDefineAbsenceTypes.getContentPane().add(lblThreshold_3);
		
		JLabel lblThreshold_4 = new JLabel("Threshold 6");
		lblThreshold_4.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 15));
		lblThreshold_4.setBounds(23, 191, 79, 14);
		frmDefineAbsenceTypes.getContentPane().add(lblThreshold_4);
	}
}
