package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileSystemView;

import dao.AbsenceDAO;
import dao.SessionDAO;
import dao.StudentDAO;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
/**
 * This is the frame allowing the students to justify their absences
 *  
 * @author DIOP-SONY
 * @version 1.0
 */
public class PutAProof_AnticipateAbsence {

	public JFrame frmPutAProof;
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Create the application.
	 */
	public PutAProof_AnticipateAbsence() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmPutAProof = new JFrame();
		frmPutAProof.setTitle("Put a Proof / Anticipate a absence");
		frmPutAProof.setBounds(100, 100, 546, 329);
		frmPutAProof.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmPutAProof.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 269, 290);
		frmPutAProof.getContentPane().add(panel);
		panel.setLayout(null);
		
		JSeparator separator = new JSeparator();
		separator.setOrientation(SwingConstants.VERTICAL);
		separator.setBounds(261, 0, 18, 290);
		panel.add(separator);
		
		JLabel lblAbsenceId = new JLabel("Absence ID");
		lblAbsenceId.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 15));
		lblAbsenceId.setBounds(10, 60, 84, 14);
		panel.add(lblAbsenceId);
		
		JComboBox absenceBox = new JComboBox();
		ArrayList<Integer> absenceList;
		if(Homepage.identifier.contains("esigelec")==false)
			absenceList = new AbsenceDAO().getAbsences(Integer.parseInt(Homepage.identifier));
		else
			absenceList = new AbsenceDAO().getAbsences(new StudentDAO().getId(Homepage.identifier));
		for(int i=0; i<absenceList.size(); i++)
			absenceBox.addItem((Integer)absenceList.get(i));
		absenceBox.setBounds(104, 57, 119, 20);
		panel.add(absenceBox);
		
		JButton button = new JButton("Cancel");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmPutAProof.hide();
				new StudentAbsences().frmShowAbsences.setVisible(true);
			}
		});
		button.setForeground(Color.BLACK);
		button.setFont(new Font("Calibri", Font.BOLD, 15));
		button.setBackground(Color.LIGHT_GRAY);
		button.setBounds(10, 256, 89, 23);
		panel.add(button);
		
		JButton btnOk = new JButton("OK");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(new AbsenceDAO().updateProof(textField.getText(), (int)absenceBox.getSelectedItem())==0)
					JOptionPane.showMessageDialog(frmPutAProof, "Database error !!!");
				else
					JOptionPane.showMessageDialog(frmPutAProof, "Successfully !!!");
			}
		});
		btnOk.setForeground(Color.BLACK);
		btnOk.setFont(new Font("Calibri", Font.BOLD, 15));
		btnOk.setBackground(new Color(0, 191, 255));
		btnOk.setBounds(87, 164, 72, 23);
		panel.add(btnOk);
		
		JButton btnPutAProof = new JButton("Put a proof");
		btnPutAProof.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// allows the selection of a document in the system
				JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
				jfc.setApproveButtonText("cliquez ici");
				
				int returnValue = jfc.showOpenDialog(null);
				if (returnValue == JFileChooser.APPROVE_OPTION) {
					File selectedFile = jfc.getSelectedFile();
				    textField.setText(selectedFile.getAbsolutePath());
				}
			}
		});
		btnPutAProof.setForeground(Color.BLACK);
		btnPutAProof.setFont(new Font("Calibri", Font.BOLD, 13));
		btnPutAProof.setBackground(new Color(32, 178, 170));
		btnPutAProof.setBounds(10, 100, 111, 23);
		panel.add(btnPutAProof);
		
		textField = new JTextField();
		textField.setBounds(131, 101, 92, 20);
		panel.add(textField);
		textField.setColumns(10);
		
		JLabel lblPutAProof = new JLabel("PUT A PROOF");
		lblPutAProof.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 20));
		lblPutAProof.setBounds(70, 11, 125, 24);
		panel.add(lblPutAProof);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(268, 0, 262, 290);
		frmPutAProof.getContentPane().add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblAnticipateAbsence = new JLabel("ANTICIPATE AN ABSENCE");
		lblAnticipateAbsence.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 20));
		lblAnticipateAbsence.setBounds(23, 11, 214, 24);
		panel_1.add(lblAnticipateAbsence);
		
		JLabel lblSessionId = new JLabel("Session ID");
		lblSessionId.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 15));
		lblSessionId.setBounds(10, 62, 84, 14);
		panel_1.add(lblSessionId);
		
		JComboBox sessionBox = new JComboBox();
		ArrayList<String> sessionList = new SessionDAO().getSessions();
		for(int i=0; i<sessionList.size(); i++)
			sessionBox.addItem((String) sessionList.get(i));
		sessionBox.setBounds(104, 59, 119, 20);
		panel_1.add(sessionBox);
		
		JButton button_1 = new JButton("Put a proof");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// allows the selection of a document in the system
				JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
				jfc.setApproveButtonText("cliquez ici");
				
				int returnValue = jfc.showOpenDialog(null);
				if (returnValue == JFileChooser.APPROVE_OPTION) {
					File selectedFile = jfc.getSelectedFile();
				    textField_1.setText(selectedFile.getAbsolutePath());
				}
			}
		});
		button_1.setForeground(Color.BLACK);
		button_1.setFont(new Font("Calibri", Font.BOLD, 13));
		button_1.setBackground(new Color(32, 178, 170));
		button_1.setBounds(10, 141, 111, 23);
		panel_1.add(button_1);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(131, 142, 92, 20);
		panel_1.add(textField_1);
		
		JComboBox absenceTypeBox = new JComboBox();
		ArrayList<String> types = new AbsenceDAO().getAbsenceTypes();
		for(int i=0; i<types.size(); i++)
			absenceTypeBox.addItem(types.get(i));
		absenceTypeBox.setBounds(104, 97, 119, 20);
		panel_1.add(absenceTypeBox);
		
		JButton button_2 = new JButton("OK");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(Homepage.identifier.contains("esigelec")==false) {
					if(new AbsenceDAO().add((String)absenceTypeBox.getSelectedItem(), Integer.parseInt(Homepage.identifier), (String)sessionBox.getSelectedItem(), textField_1.getText())!=0)
						JOptionPane.showMessageDialog(frmPutAProof, "Successfully !!!");
					else
						JOptionPane.showMessageDialog(frmPutAProof, "Database error !!!");
				}
				else {
					if(new AbsenceDAO().add((String)absenceTypeBox.getSelectedItem(), new StudentDAO().getId(Homepage.identifier), (String)sessionBox.getSelectedItem(), textField_1.getText())!=0)
						JOptionPane.showMessageDialog(frmPutAProof, "Successfully !!!");
					else
						JOptionPane.showMessageDialog(frmPutAProof, "Database error !!!");
				}
			}
		});
		button_2.setForeground(Color.BLACK);
		button_2.setFont(new Font("Calibri", Font.BOLD, 15));
		button_2.setBackground(new Color(0, 191, 255));
		button_2.setBounds(94, 211, 72, 23);
		panel_1.add(button_2);
		
		JLabel lblAbsenceType = new JLabel("Absence type");
		lblAbsenceType.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 15));
		lblAbsenceType.setBounds(10, 100, 84, 14);
		panel_1.add(lblAbsenceType);
		
		
	}
}
