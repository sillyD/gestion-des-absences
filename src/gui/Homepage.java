package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Window.Type;
import java.awt.Toolkit;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.JTextField;

import dao.AuthenticationDAO;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.SystemColor;
import javax.swing.JPasswordField;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import java.awt.event.KeyAdapter;
/**
 * This is the home frame
 * 
 * @author DIOP-SONY
 * @version 1.0
 */
public class Homepage {

	public JFrame frmAcceuil;
	private JTextField identifierField;
	public JPasswordField passwordField;
	/**
	 * used to get the username elsewhere in the program
	 */
	public static String identifier;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Homepage window = new Homepage();
					window.frmAcceuil.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Homepage() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmAcceuil = new JFrame();
		frmAcceuil.setResizable(false);
		frmAcceuil.getContentPane().setBackground(SystemColor.control);
		frmAcceuil.setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\zlidi\\Desktop\\images projet\\icone accueil.ico"));
		frmAcceuil.setTitle("HomePage\r\n");
		frmAcceuil.setBounds(100, 100, 532, 300);
		frmAcceuil.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmAcceuil.setLocationRelativeTo(null);
		frmAcceuil.getContentPane().setLayout(null);
		
		JLabel lblBienvenueDansNotre = new JLabel("WELCOME TO OUR ABSENCE MANAGEMENT SOFTWARE");
		lblBienvenueDansNotre.setForeground(new Color(255, 255, 255));
		lblBienvenueDansNotre.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 20));
		lblBienvenueDansNotre.setBounds(30, 11, 486, 54);
		frmAcceuil.getContentPane().add(lblBienvenueDansNotre);
		
		JLabel lblIdentifiantmailOuId = new JLabel("Username(Mail ou Id)");
		lblIdentifiantmailOuId.setForeground(new Color(255, 255, 255));
		lblIdentifiantmailOuId.setFont(new Font("Calibri", Font.BOLD, 16));
		lblIdentifiantmailOuId.setBounds(204, 109, 155, 17);
		frmAcceuil.getContentPane().add(lblIdentifiantmailOuId);
		
		identifierField = new JTextField();
		identifierField.setBounds(369, 107, 105, 20);
		frmAcceuil.getContentPane().add(identifierField);
		identifierField.setColumns(10);
		
		JLabel lblMotDePasse = new JLabel("Password");
		lblMotDePasse.setForeground(new Color(255, 255, 255));
		lblMotDePasse.setFont(new Font("Calibri", Font.BOLD, 16));
		lblMotDePasse.setBounds(204, 149, 133, 17);
		frmAcceuil.getContentPane().add(lblMotDePasse);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(369, 147, 105, 20);
		frmAcceuil.getContentPane().add(passwordField);
		
		JButton btnSeConnecter = new JButton("Log in");
		btnSeConnecter.setFont(new Font("Calibri", Font.BOLD, 14));
		btnSeConnecter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				identifier = identifierField.getText();
				String user = new AuthenticationDAO().verify(identifierField.getText(), passwordField.getText());
				
				if(user == null)
					JOptionPane.showMessageDialog(null, "Invalid username or password !!!\n Try again!\n You can verify also your caps-lock");
				else {
					switch (user) {
					
					case "manager":
						frmAcceuil.hide();
						new ManagerChoice().frmMenuDuGestionnaire.setVisible(true);
						break;
						
					case "student":
						frmAcceuil.hide();
						new StudentChoice().frmStudentMenu.setVisible(true);
						break;
						
					case "teacher":
						frmAcceuil.hide();
						new TeacherChoice().frmTeacherMenu.setVisible(true);
						break;
						
					default:
						JOptionPane.showMessageDialog(null, "Invalid username or password !!!\n Try again!");
						break;
					}
				}
			}
		});
		btnSeConnecter.setBackground(new Color(50, 205, 50));
		btnSeConnecter.setBounds(224, 227, 112, 23);
		frmAcceuil.getContentPane().add(btnSeConnecter);
		frmAcceuil.getRootPane().setDefaultButton(btnSeConnecter); // btnSeConnecter is the default when the enter key is pressed
		
		JButton btnQuitter = new JButton("Quit");
		btnQuitter.setFont(new Font("Calibri", Font.BOLD, 14));
		btnQuitter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmAcceuil.dispose();
			}
		});
		btnQuitter.setBackground(new Color(255, 127, 80));
		btnQuitter.setBounds(367, 227, 97, 23);
		frmAcceuil.getContentPane().add(btnQuitter);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon("C:\\Users\\zlidi\\eclipse-workspace\\Gestion des absences\\Images et icones\\21_Esigelec.jpg"));
		lblNewLabel.setBounds(0, 0, 526, 271);
		frmAcceuil.getContentPane().add(lblNewLabel);
	}
}
