package gui;

import model.*;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.SystemColor;
import java.awt.Color;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileSystemView;

import dao.StudentDAO;
import dao.TeacherDAO;
import dao.UnitDAO;

import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
/**
 * This is the frame used to create, update or delete a unit, a teacher or a student by the manager
 *  
 * @author DIOP-SONY
 * @version 1.0
 */
public class UnitTeacherStudentManaging {

	public JFrame frmGestionDeCours;
	private JTextField textField_nomCours;
	private JTextField textField_masseHoraire;
	private JTextField textField_heuresTD;
	private JTextField textField_heuresTP;
	private JTextField textField_heuresAMPHI;
	private JTextField textField_heuresEXAM;
	private JTextField textField_nomEnseignant;
	private JTextField textField_prenomEnseignant;
	private JTextField textField_mailEnseignant;
	private JTextField textField_passwordEnseignant;
	private JTextField textField_telEnseignant;
	private JTextField textField_nomEtudiant;
	private JTextField textField_prenomEtudiant;
	private JTextField textField_mailEtudiant;
	private JTextField textField_passwordEtudiant;
	private JTextField textField_planning;

	/**
	 * Create the application.
	 */
	public UnitTeacherStudentManaging() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmGestionDeCours = new JFrame();
		frmGestionDeCours.setResizable(false);
		frmGestionDeCours.setTitle("Gestion de Cours, d'enseignant et d'etudiant");
		frmGestionDeCours.setBounds(100, 100, 606, 358);
		frmGestionDeCours.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmGestionDeCours.setLocationRelativeTo(null);
		frmGestionDeCours.getContentPane().setLayout(null);
		
		// Unit Field
		JLabel lblCours = new JLabel("UNIT");
		lblCours.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 14));
		lblCours.setBounds(64, 11, 46, 14);
		frmGestionDeCours.getContentPane().add(lblCours);
		
		JLabel lblNom = new JLabel("Name\r\n");
		lblNom.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNom.setBounds(10, 36, 46, 14);
		frmGestionDeCours.getContentPane().add(lblNom);
		
		textField_nomCours = new JTextField();
		textField_nomCours.setText("");
		textField_nomCours.setBounds(58, 33, 116, 23);
		frmGestionDeCours.getContentPane().add(textField_nomCours);
		textField_nomCours.setColumns(10);
		
		JLabel lblMasseHoraire = new JLabel("Hourly mass");
		lblMasseHoraire.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblMasseHoraire.setBounds(10, 64, 86, 14);
		frmGestionDeCours.getContentPane().add(lblMasseHoraire);
		
		textField_masseHoraire = new JTextField();
		textField_masseHoraire.setText("0");
		textField_masseHoraire.setColumns(10);
		textField_masseHoraire.setBounds(88, 61, 86, 23);
		frmGestionDeCours.getContentPane().add(textField_masseHoraire);
		
		JLabel lblHeuresTd = new JLabel("TD Hours");
		lblHeuresTd.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblHeuresTd.setBounds(10, 89, 86, 14);
		frmGestionDeCours.getContentPane().add(lblHeuresTd);
		
		textField_heuresTD = new JTextField();
		textField_heuresTD.setText("0");
		textField_heuresTD.setColumns(10);
		textField_heuresTD.setBounds(88, 86, 86, 23);
		frmGestionDeCours.getContentPane().add(textField_heuresTD);
		
		JLabel lblHeuresTp = new JLabel("TP Hours");
		lblHeuresTp.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblHeuresTp.setBounds(10, 114, 86, 14);
		frmGestionDeCours.getContentPane().add(lblHeuresTp);
		
		JLabel lblHeuresAmphi = new JLabel("AMPHI hours");
		lblHeuresAmphi.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblHeuresAmphi.setBounds(10, 139, 86, 14);
		frmGestionDeCours.getContentPane().add(lblHeuresAmphi);
		
		JLabel lblHeuresExam = new JLabel("EXAM hours");
		lblHeuresExam.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblHeuresExam.setBounds(10, 164, 86, 14);
		frmGestionDeCours.getContentPane().add(lblHeuresExam);
		
		textField_heuresTP = new JTextField();
		textField_heuresTP.setText("0");
		textField_heuresTP.setColumns(10);
		textField_heuresTP.setBounds(88, 111, 86, 23);
		frmGestionDeCours.getContentPane().add(textField_heuresTP);
		
		textField_heuresAMPHI = new JTextField();
		textField_heuresAMPHI.setText("0");
		textField_heuresAMPHI.setColumns(10);
		textField_heuresAMPHI.setBounds(88, 136, 86, 23);
		frmGestionDeCours.getContentPane().add(textField_heuresAMPHI);
		
		textField_heuresEXAM = new JTextField();
		textField_heuresEXAM.setText("0");
		textField_heuresEXAM.setColumns(10);
		textField_heuresEXAM.setBounds(88, 161, 86, 23);
		frmGestionDeCours.getContentPane().add(textField_heuresEXAM);
		
		JButton btnAjouterCours = new JButton("Add");
		btnAjouterCours.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// we verify if the hourlyMass is equal to the sum of TD, TP, AMPHI and EXAM hours and if the unit is registered
				if(Integer.parseInt(textField_masseHoraire.getText())==Integer.parseInt(textField_heuresTD.getText())+Integer.parseInt(textField_heuresTP.getText())+Integer.parseInt(textField_heuresAMPHI.getText())+Integer.parseInt(textField_heuresEXAM.getText())) {
					if(new UnitDAO().add(new Unit(textField_nomCours.getText(), Integer.parseInt(textField_masseHoraire.getText()), Integer.parseInt(textField_heuresTD.getText()), Integer.parseInt(textField_heuresTP.getText()), Integer.parseInt(textField_heuresAMPHI.getText()), Integer.parseInt(textField_heuresEXAM.getText())))!=0) 
						JOptionPane.showMessageDialog(frmGestionDeCours, "Successfully !");
					else
						JOptionPane.showMessageDialog(frmGestionDeCours, "Database Error !");
				}
				else
					JOptionPane.showMessageDialog(frmGestionDeCours, "The hourly mass is not equals to the sum of the others hours !!!");
				textField_nomCours.setText("");
				textField_masseHoraire.setText("0");
				textField_heuresTD.setText("0");
				textField_heuresTP.setText("0");
				textField_heuresAMPHI.setText("0");
				textField_heuresEXAM.setText("0");
			}
		});
		btnAjouterCours.setBackground(SystemColor.activeCaption);
		btnAjouterCours.setBounds(23, 213, 77, 23);
		frmGestionDeCours.getContentPane().add(btnAjouterCours);
		
		JButton btnModifierCours = new JButton("Update");
		btnModifierCours.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(Integer.parseInt(textField_masseHoraire.getText())==Integer.parseInt(textField_heuresTD.getText())+Integer.parseInt(textField_heuresTP.getText())+Integer.parseInt(textField_heuresAMPHI.getText())+Integer.parseInt(textField_heuresEXAM.getText())) {
					if(new UnitDAO().update(new Unit(textField_nomCours.getText(), Integer.parseInt(textField_masseHoraire.getText()), Integer.parseInt(textField_heuresTD.getText()), Integer.parseInt(textField_heuresTP.getText()), Integer.parseInt(textField_heuresAMPHI.getText()), Integer.parseInt(textField_heuresEXAM.getText())))!=0) 
						JOptionPane.showMessageDialog(frmGestionDeCours, "Successfully !");
					else
						JOptionPane.showMessageDialog(frmGestionDeCours, "Database Error !");
				}
				else
					JOptionPane.showMessageDialog(frmGestionDeCours, "The hourly mass is not equals to the sum of the others hours !!!");
				textField_nomCours.setText("");
				textField_masseHoraire.setText("0");
				textField_heuresTD.setText("0");
				textField_heuresTP.setText("0");
				textField_heuresAMPHI.setText("0");
				textField_heuresEXAM.setText("0");
			}
		});
		btnModifierCours.setBackground(new Color(255, 165, 0));
		btnModifierCours.setBounds(64, 239, 77, 23);
		frmGestionDeCours.getContentPane().add(btnModifierCours);
		
		JButton btnSupprimerCours = new JButton("Delete");
		btnSupprimerCours.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// we verify if the unit has been deleted
				if(new UnitDAO().delete(new Unit(textField_nomCours.getText(), Integer.parseInt(textField_masseHoraire.getText()), Integer.parseInt(textField_heuresTD.getText()), Integer.parseInt(textField_heuresTP.getText()), Integer.parseInt(textField_heuresAMPHI.getText()), Integer.parseInt(textField_heuresEXAM.getText())))!=0)
					JOptionPane.showMessageDialog(frmGestionDeCours, "Successfully !");
				else
					JOptionPane.showMessageDialog(frmGestionDeCours, "Database Error !");
				textField_nomCours.setText("");
				textField_masseHoraire.setText("0");
				textField_heuresTD.setText("0");
				textField_heuresTP.setText("0");
				textField_heuresAMPHI.setText("0");
				textField_heuresEXAM.setText("0");
			}
		});
		btnSupprimerCours.setBackground(new Color(255, 0, 0));
		btnSupprimerCours.setBounds(106, 213, 86, 23);
		frmGestionDeCours.getContentPane().add(btnSupprimerCours);
		
		JSeparator separator = new JSeparator();
		separator.setOrientation(SwingConstants.VERTICAL);
		separator.setBounds(202, 11, 15, 239);
		frmGestionDeCours.getContentPane().add(separator);
		
		// Teacher Field
		JLabel lblEnseignant = new JLabel("TEACHER");
		lblEnseignant.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 14));
		lblEnseignant.setBounds(258, 11, 77, 14);
		frmGestionDeCours.getContentPane().add(lblEnseignant);
		
		JLabel lblName = new JLabel("LastName");
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblName.setBounds(217, 37, 51, 14);
		frmGestionDeCours.getContentPane().add(lblName);
		
		textField_nomEnseignant = new JTextField();
		textField_nomEnseignant.setText("");
		textField_nomEnseignant.setColumns(10);
		textField_nomEnseignant.setBounds(278, 33, 95, 23);
		frmGestionDeCours.getContentPane().add(textField_nomEnseignant);
		
		JLabel lblPrenom = new JLabel("FirstName");
		lblPrenom.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblPrenom.setBounds(217, 65, 60, 14);
		frmGestionDeCours.getContentPane().add(lblPrenom);
		
		JLabel lblMail = new JLabel("Mail");
		lblMail.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblMail.setBounds(217, 90, 46, 14);
		frmGestionDeCours.getContentPane().add(lblMail);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblPassword.setBounds(217, 115, 60, 14);
		frmGestionDeCours.getContentPane().add(lblPassword);
		
		JLabel lblTelephone = new JLabel("Phone");
		lblTelephone.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblTelephone.setBounds(217, 140, 60, 14);
		frmGestionDeCours.getContentPane().add(lblTelephone);
		
		textField_prenomEnseignant = new JTextField();
		textField_prenomEnseignant.setText("");
		textField_prenomEnseignant.setColumns(10);
		textField_prenomEnseignant.setBounds(278, 61, 95, 23);
		frmGestionDeCours.getContentPane().add(textField_prenomEnseignant);
		
		textField_mailEnseignant = new JTextField();
		textField_mailEnseignant.setText("@esigelec.fr");
		textField_mailEnseignant.setColumns(10);
		textField_mailEnseignant.setBounds(268, 86, 105, 23);
		frmGestionDeCours.getContentPane().add(textField_mailEnseignant);
		
		textField_passwordEnseignant = new JTextField();
		textField_passwordEnseignant.setText("");
		textField_passwordEnseignant.setColumns(10);
		textField_passwordEnseignant.setBounds(278, 111, 95, 23);
		frmGestionDeCours.getContentPane().add(textField_passwordEnseignant);
		
		JLabel show_error = new JLabel("");
		show_error.setFont(new Font("Tahoma", Font.PLAIN, 10));
		show_error.setForeground(new Color(255, 0, 0));
		show_error.setBounds(213, 164, 183, 14);
		frmGestionDeCours.getContentPane().add(show_error);
		
		textField_telEnseignant = new JTextField();
		textField_telEnseignant.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				try {
					int i = Integer.parseInt(textField_telEnseignant.getText());
					show_error.setText("");
				} catch(NumberFormatException nfe) {
					show_error.setText("Please enter numbers for the phone!!!");
				}
			}
		});
		textField_telEnseignant.setText("");
		textField_telEnseignant.setColumns(10);
		textField_telEnseignant.setBounds(278, 137, 95, 23);
		frmGestionDeCours.getContentPane().add(textField_telEnseignant);
		
		textField_planning = new JTextField();
		textField_planning.setText("");
		textField_planning.setColumns(10);
		textField_planning.setBounds(313, 179, 83, 23);
		frmGestionDeCours.getContentPane().add(textField_planning);
		
		JButton buttonAjoutEnseignant = new JButton("Add");
		buttonAjoutEnseignant.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// we verify if the teacher has been added
				if (new TeacherDAO().add(new Teacher(textField_nomEnseignant.getText(), textField_prenomEnseignant.getText(), textField_mailEnseignant.getText(), textField_passwordEnseignant.getText(), textField_telEnseignant.getText()), textField_planning.getText())!=0)
					JOptionPane.showMessageDialog(frmGestionDeCours, "Successfully !");
				else
					JOptionPane.showMessageDialog(frmGestionDeCours, "Database Error !");
				textField_nomEnseignant.setText("");
				textField_prenomEnseignant.setText("");
				textField_mailEnseignant.setText("@esigelec.fr");
				textField_passwordEnseignant.setText("");
				textField_telEnseignant.setText("");
				textField_planning.setText("");
			}
		});
		buttonAjoutEnseignant.setBackground(SystemColor.activeCaption);
		buttonAjoutEnseignant.setBounds(223, 213, 77, 23);
		frmGestionDeCours.getContentPane().add(buttonAjoutEnseignant);
		
		JButton buttonSupprEnseignant = new JButton("Delete");
		buttonSupprEnseignant.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// we verify if the teacher has been deleted
				if (new TeacherDAO().delete(new Teacher(textField_nomEnseignant.getText(), textField_prenomEnseignant.getText(), textField_mailEnseignant.getText(), textField_passwordEnseignant.getText(), textField_telEnseignant.getText()))!=0)
					JOptionPane.showMessageDialog(frmGestionDeCours, "Successfully !");
				else
					JOptionPane.showMessageDialog(frmGestionDeCours, "Database Error !");
				textField_nomEnseignant.setText("");
				textField_prenomEnseignant.setText("");
				textField_mailEnseignant.setText("@esigelec.fr");
				textField_passwordEnseignant.setText("");
				textField_telEnseignant.setText("");
				textField_planning.setText("");
			}
		});
		buttonSupprEnseignant.setBackground(Color.RED);
		buttonSupprEnseignant.setBounds(310, 213, 86, 23);
		frmGestionDeCours.getContentPane().add(buttonSupprEnseignant);
		
		JButton buttonModifEnseignant = new JButton("Update");
		buttonModifEnseignant.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// we verify if the teacher has been updated
				if (new TeacherDAO().update(new Teacher(textField_nomEnseignant.getText(), textField_prenomEnseignant.getText(), textField_mailEnseignant.getText(), textField_passwordEnseignant.getText(), textField_telEnseignant.getText()), textField_planning.getText())!=0)
					JOptionPane.showMessageDialog(frmGestionDeCours, "Successfully !");
				else
					JOptionPane.showMessageDialog(frmGestionDeCours, "Database Error !");
				textField_nomEnseignant.setText("");
				textField_prenomEnseignant.setText("");
				textField_mailEnseignant.setText("@esigelec.fr");
				textField_passwordEnseignant.setText("");
				textField_telEnseignant.setText("");
				textField_planning.setText("");
			}
		});
		buttonModifEnseignant.setBackground(new Color(255, 165, 0));
		buttonModifEnseignant.setBounds(263, 239, 86, 23);
		frmGestionDeCours.getContentPane().add(buttonModifEnseignant);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setOrientation(SwingConstants.VERTICAL);
		separator_1.setBounds(406, 11, 15, 239);
		frmGestionDeCours.getContentPane().add(separator_1);
		
		// Student Field
		JLabel lblEtudiant = new JLabel("STUDENT");
		lblEtudiant.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 14));
		lblEtudiant.setBounds(460, 11, 77, 14);
		frmGestionDeCours.getContentPane().add(lblEtudiant);
		
		JLabel lblSurname = new JLabel("LastName");
		lblSurname.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblSurname.setBounds(415, 36, 64, 14);
		frmGestionDeCours.getContentPane().add(lblSurname);
		
		JLabel lblFirstname = new JLabel("FirstName");
		lblFirstname.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblFirstname.setBounds(415, 65, 60, 14);
		frmGestionDeCours.getContentPane().add(lblFirstname);
		
		JLabel label_3 = new JLabel("Mail");
		label_3.setFont(new Font("Tahoma", Font.PLAIN, 12));
		label_3.setBounds(415, 89, 46, 14);
		frmGestionDeCours.getContentPane().add(label_3);
		
		JLabel label_4 = new JLabel("Password");
		label_4.setFont(new Font("Tahoma", Font.PLAIN, 12));
		label_4.setBounds(415, 114, 60, 14);
		frmGestionDeCours.getContentPane().add(label_4);
		
		JLabel lblFiliere = new JLabel("Sector");
		lblFiliere.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblFiliere.setBounds(415, 139, 60, 14);
		frmGestionDeCours.getContentPane().add(lblFiliere);
		
		textField_nomEtudiant = new JTextField();
		textField_nomEtudiant.setText("");
		textField_nomEtudiant.setColumns(10);
		textField_nomEtudiant.setBounds(480, 33, 95, 23);
		frmGestionDeCours.getContentPane().add(textField_nomEtudiant);
		
		textField_prenomEtudiant = new JTextField();
		textField_prenomEtudiant.setText("");
		textField_prenomEtudiant.setColumns(10);
		textField_prenomEtudiant.setBounds(480, 62, 95, 23);
		frmGestionDeCours.getContentPane().add(textField_prenomEtudiant);
		
		textField_mailEtudiant = new JTextField();
		textField_mailEtudiant.setText("@groupe-esigelec.org");
		textField_mailEtudiant.setColumns(10);
		textField_mailEtudiant.setBounds(480, 87, 95, 23);
		frmGestionDeCours.getContentPane().add(textField_mailEtudiant);
		
		textField_passwordEtudiant = new JTextField();
		textField_passwordEtudiant.setText("");
		textField_passwordEtudiant.setColumns(10);
		textField_passwordEtudiant.setBounds(480, 112, 95, 23);
		frmGestionDeCours.getContentPane().add(textField_passwordEtudiant);
		
		JComboBox comboBoxFiliere = new JComboBox();
		comboBoxFiliere.setModel(new DefaultComboBoxModel(new String[] {"Classic", "Learning"}));
		comboBoxFiliere.setBounds(480, 137, 105, 20);
		frmGestionDeCours.getContentPane().add(comboBoxFiliere);
		
		JButton buttonAjoutEtudiant = new JButton("Add");
		buttonAjoutEtudiant.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(new StudentDAO().add(new Student(textField_nomEtudiant.getText(), textField_prenomEtudiant.getText(), textField_mailEtudiant.getText(), textField_passwordEtudiant.getText(), (String) comboBoxFiliere.getSelectedItem(), null, null, null, 0))!=0)
					JOptionPane.showMessageDialog(frmGestionDeCours, "Successfully !");
				else
					JOptionPane.showMessageDialog(frmGestionDeCours, "Database Error !");		
				textField_nomEtudiant.setText("");
				textField_prenomEtudiant.setText("");
				textField_mailEtudiant.setText("@groupe-esigelec.org");
				textField_passwordEtudiant.setText("");
			}
		});
		buttonAjoutEtudiant.setBackground(SystemColor.activeCaption);
		buttonAjoutEtudiant.setBounds(415, 213, 77, 23);
		frmGestionDeCours.getContentPane().add(buttonAjoutEtudiant);
		
		JButton buttonSupprEtudiant = new JButton("Delete");
		buttonSupprEtudiant.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(new StudentDAO().delete(new Student(textField_nomEtudiant.getText(), textField_prenomEtudiant.getText(), textField_mailEtudiant.getText(), textField_passwordEtudiant.getText(), (String) comboBoxFiliere.getSelectedItem(), null, null, null, 0))!=0)
					JOptionPane.showMessageDialog(frmGestionDeCours, "Successfully !");
				else
					JOptionPane.showMessageDialog(frmGestionDeCours, "Database Error !");
				textField_nomEtudiant.setText("");
				textField_prenomEtudiant.setText("");
				textField_mailEtudiant.setText("@groupe-esigelec.org");
				textField_passwordEtudiant.setText("");
			}
		});
		buttonSupprEtudiant.setBackground(Color.RED);
		buttonSupprEtudiant.setBounds(502, 213, 86, 23);
		frmGestionDeCours.getContentPane().add(buttonSupprEtudiant);
		
		JButton buttonModifEtudiant = new JButton("Update");
		buttonModifEtudiant.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(new StudentDAO().update(new Student(textField_nomEtudiant.getText(), textField_prenomEtudiant.getText(), textField_mailEtudiant.getText(), textField_passwordEtudiant.getText(), (String) comboBoxFiliere.getSelectedItem(), null, null, null, 0))!=0)
					JOptionPane.showMessageDialog(frmGestionDeCours, "Successfully !");
				else
					JOptionPane.showMessageDialog(frmGestionDeCours, "Database Error !");	
				textField_nomEtudiant.setText("");
				textField_prenomEtudiant.setText("");
				textField_mailEtudiant.setText("@groupe-esigelec.org");
				textField_passwordEtudiant.setText("");
			}
		});
		buttonModifEtudiant.setBackground(new Color(255, 165, 0));
		buttonModifEtudiant.setBounds(462, 239, 86, 23);
		frmGestionDeCours.getContentPane().add(buttonModifEtudiant);
		
		JButton btnRetour = new JButton("Back");
		btnRetour.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmGestionDeCours.hide();
				new ManagerChoice().frmMenuDuGestionnaire.setVisible(true);
			}
		});
		btnRetour.setBackground(new Color(0, 255, 0));
		btnRetour.setBounds(496, 298, 89, 23);
		frmGestionDeCours.getContentPane().add(btnRetour);
		
		JButton btnPutPlanning = new JButton("Put planning\r\n");
		btnPutPlanning.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// allows the selection of a document in the system
				JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
				jfc.setApproveButtonText("cliquez ici");
				
				int returnValue = jfc.showOpenDialog(null);
				if (returnValue == JFileChooser.APPROVE_OPTION) {
					File selectedFile = jfc.getSelectedFile();
				    textField_planning.setText(selectedFile.getAbsolutePath());
				}
			}
		});
		btnPutPlanning.setBackground(new Color(30, 144, 255));
		btnPutPlanning.setBounds(207, 182, 96, 20);
		frmGestionDeCours.getContentPane().add(btnPutPlanning);
	}
}
