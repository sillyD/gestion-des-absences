package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JList;
import javax.swing.JOptionPane;

import java.awt.Color;
import javax.swing.AbstractListModel;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JCheckBoxMenuItem;
import java.awt.List;
import javax.swing.UIManager;
import dao.StudentDAO;
import dao.StudentsGroupDAO;

import javax.swing.JRadioButton;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.ScrollPane;
import java.awt.Panel;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JDesktopPane;
import javax.swing.JScrollPane;
import java.awt.CardLayout;
import javax.swing.JPanel;
/**
 * This is the second frame for affecting the student in groups
 * 
 * @author DIOP-SONY
 * @version 1.0
 */
public class AffectStudent2 {

	public JFrame frmAffectStudentsIn;
	private JTextField textField;

	/**
	 * Create the application.
	 */
	public AffectStudent2() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmAffectStudentsIn = new JFrame();
		frmAffectStudentsIn.setLocationRelativeTo(null);
		frmAffectStudentsIn.getContentPane().setBackground(new Color(220, 220, 220));
		frmAffectStudentsIn.getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(32, 24, 200, 226);
		frmAffectStudentsIn.getContentPane().add(scrollPane);
		
		JPanel panel = new JPanel();
		scrollPane.setViewportView(panel);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		ArrayList<String> studentList = new StudentDAO().getListeEleve(AffectStudent1.numGroup);
		String[] nameSplit;
		JCheckBox[] checkboxes = new JCheckBox[studentList.size()];
		for(int i=0; i<checkboxes.length; i++) {
			nameSplit = studentList.get(i).split("-");
			checkboxes[i] = new JCheckBox(nameSplit[0]+" "+nameSplit[1]);
			checkboxes[i].setFont(new Font("Calibri", Font.BOLD, 12));
		}
		
		for(int i=0; i<checkboxes.length; i++)
			panel.add(checkboxes[i]);
		
		JButton btnValider = new JButton("OK");
		btnValider.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String getID[];
				for(int i=0; i<checkboxes.length; i++) {
					if(checkboxes[i].isSelected()==true) {
						getID = studentList.get(i).split("-");
						if(new StudentDAO().updateNumGroupe(AffectStudent1.numGroup, Integer.parseInt(getID[2]))==0)
							JOptionPane.showMessageDialog(frmAffectStudentsIn, "Database Error\n Try again !");
					}
				}
				JOptionPane.showMessageDialog(frmAffectStudentsIn, "Successfully");
				// we disable all the checkboxes
				for(int i=0; i<checkboxes.length; i++)
					checkboxes[i].setSelected(false);;
			}
		});
		btnValider.setBackground(new Color(30, 144, 255));
		btnValider.setFont(new Font("Calibri", Font.BOLD, 13));
		btnValider.setBounds(388, 158, 89, 23);
		frmAffectStudentsIn.getContentPane().add(btnValider);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmAffectStudentsIn.hide();
				new GroupManaging().frmGestionDeGroupes.setVisible(true);
			}
		});
		btnCancel.setBackground(new Color(192, 192, 192));
		btnCancel.setFont(new Font("Calibri", Font.BOLD, 13));
		btnCancel.setBounds(273, 158, 89, 23);
		frmAffectStudentsIn.getContentPane().add(btnCancel);
		
		frmAffectStudentsIn.setTitle("AFFECT STUDENTS IN A GROUP");
		frmAffectStudentsIn.setBounds(100, 100, 532, 300);
		frmAffectStudentsIn.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		
	}
}
