package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;

import dao.StudentsGroupDAO;

import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
/**
 * This is the first frame to make the call
 *  
 * @author DIOP-SONY
 * @version 1.0
 */
public class MakeTheCall1 {

	public JFrame frmMakeTheCall1;
	public static String groupNumber;

	/**
	 * Create the application.
	 */
	public MakeTheCall1() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmMakeTheCall1 = new JFrame();
		frmMakeTheCall1.setTitle("MAKE THE CALL_1");
		frmMakeTheCall1.setBounds(100, 100, 450, 300);
		frmMakeTheCall1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMakeTheCall1.setLocationRelativeTo(null);
		frmMakeTheCall1.getContentPane().setLayout(null);
		
		JLabel lblGroupNumber = new JLabel("Group Number");
		lblGroupNumber.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 17));
		lblGroupNumber.setBounds(77, 90, 127, 14);
		frmMakeTheCall1.getContentPane().add(lblGroupNumber);
		
		JComboBox comboBox = new JComboBox();
		ArrayList<String> groups = new StudentsGroupDAO().getUnitGroups();
		for(int i=0; i<groups.size(); i++)
			comboBox.addItem(groups.get(i));
		comboBox.setBounds(214, 87, 98, 20);
		frmMakeTheCall1.getContentPane().add(comboBox);
		
		JButton btnOk = new JButton("OK");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(new StudentsGroupDAO().SearchGroup((String)comboBox.getSelectedItem())==0)
					JOptionPane.showMessageDialog(frmMakeTheCall1, "This group doesn't exist");
				else {
					groupNumber = (String)comboBox.getSelectedItem();
					frmMakeTheCall1.hide();
					new MakeTheCall2().frmMakeTheCall.setVisible(true);
				}
			}
		});
		btnOk.setBackground(new Color(30, 144, 255));
		btnOk.setForeground(new Color(0, 0, 0));
		btnOk.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnOk.setBounds(103, 184, 89, 23);
		frmMakeTheCall1.getContentPane().add(btnOk);
		frmMakeTheCall1.getRootPane().setDefaultButton(btnOk);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmMakeTheCall1.hide();
				new TeacherChoice().frmTeacherMenu.setVisible(true);
			}
		});
		btnCancel.setBackground(new Color(192, 192, 192));
		btnCancel.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnCancel.setBounds(223, 184, 89, 23);
		frmMakeTheCall1.getContentPane().add(btnCancel);
	}
}
