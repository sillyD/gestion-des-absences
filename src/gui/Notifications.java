package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import dao.ConnectionDAO;
import dao.StudentDAO;
import net.proteanit.sql.DbUtils;

import javax.swing.JButton;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
/**
 * This is the frame used to show the notifications of a student
 *  
 * @author DIOP-SONY
 * @version 1.0
 */
public class Notifications extends ConnectionDAO{

	public JFrame frmNotifications;
	private JTable table;

	/**
	 * Create the application.
	 */
	public Notifications() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmNotifications = new JFrame();
		frmNotifications.setTitle("NOTIFICATIONS");
		frmNotifications.setBounds(100, 100, 598, 300);
		frmNotifications.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmNotifications.getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 562, 142);
		frmNotifications.getContentPane().add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		try {
			Connection con = DriverManager.getConnection(URL, LOGIN, PASS);
			PreparedStatement ps = con.prepareStatement("SELECT message, dateEnvoi FROM Notifications WHERE idEtudiant = ?");
			if(Homepage.identifier.contains("esigelec")==false)
				ps.setInt(1, Integer.parseInt(Homepage.identifier));
			else
				ps.setInt(1, new StudentDAO().getId(Homepage.identifier));
			ResultSet rs = ps.executeQuery();
			table.setModel(DbUtils.resultSetToTableModel(rs));
			table.setEnabled(false);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frmNotifications.hide();
				new StudentChoice().frmStudentMenu.setVisible(true);
			}
		});
		btnCancel.setFont(new Font("Calibri", Font.BOLD, 13));
		btnCancel.setBackground(Color.LIGHT_GRAY);
		btnCancel.setBounds(10, 227, 89, 23);
		frmNotifications.getContentPane().add(btnCancel);
		
		JLabel lblTheDateIs = new JLabel("The date is in format:  YYYY-MM-DD HH-MM-SS");
		lblTheDateIs.setForeground(Color.RED);
		lblTheDateIs.setBounds(199, 178, 295, 14);
		frmNotifications.getContentPane().add(lblTheDateIs);
	}
}
