package gui;

import java.awt.*;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import com.toedter.calendar.JDateChooser;

import dao.AbsenceDAO;
import dao.SessionDAO;
import dao.StudentsGroupDAO;
import dao.TeacherDAO;
import model.Absence;
import model.Session;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
/**
 * This is used to create a TD session or a TP session
 *  
 * @author DIOP-SONY
 * @version 1.0
 */
public class CreateSessionTD_TP {

	public JFrame frmCreateSessionsTD_TP;
	private JTextField jtfSessionID;
	private JTextField jtfSchedule;
	private JTextField jtfDuration;
	private JTextField jtfUnit;
	private JTextField jtfClassroom;
	private JTextField jtfNbrAbsences;

	/**
	 * Create the application.
	 */
	public CreateSessionTD_TP() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCreateSessionsTD_TP = new JFrame();
		frmCreateSessionsTD_TP.setTitle("CREATE SESSIONS TD_TP");
		frmCreateSessionsTD_TP.setBounds(100, 100, 584, 394);
		frmCreateSessionsTD_TP.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmCreateSessionsTD_TP.setLocationRelativeTo(null);
		frmCreateSessionsTD_TP.getContentPane().setLayout(new BoxLayout(frmCreateSessionsTD_TP.getContentPane(), BoxLayout.X_AXIS));
		
		JPanel panel = new JPanel();
		frmCreateSessionsTD_TP.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblSessionid = new JLabel("Session'ID");
		lblSessionid.setFont(new Font("Calibri", Font.BOLD, 16));
		lblSessionid.setBounds(24, 34, 81, 14);
		panel.add(lblSessionid);
		
		jtfSessionID = new JTextField("Exple: Programmer en JAVA_S15");
		jtfSessionID.setBounds(112, 31, 162, 20);
		panel.add(jtfSessionID);
		jtfSessionID.setColumns(10);
		
		JLabel lblDate = new JLabel("Date");
		lblDate.setFont(new Font("Calibri", Font.BOLD, 16));
		lblDate.setBounds(24, 87, 81, 14);
		panel.add(lblDate);
		
		JLabel lblSchedule = new JLabel("Schedule");
		lblSchedule.setFont(new Font("Calibri", Font.BOLD, 16));
		lblSchedule.setBounds(24, 145, 81, 14);
		panel.add(lblSchedule);
		
		JLabel lblDuration = new JLabel("Duration(hour)");
		lblDuration.setFont(new Font("Calibri", Font.BOLD, 16));
		lblDuration.setBounds(10, 206, 111, 14);
		panel.add(lblDuration);
		
		JLabel lblType = new JLabel("Type");
		lblType.setFont(new Font("Calibri", Font.BOLD, 16));
		lblType.setBounds(24, 262, 81, 14);
		panel.add(lblType);
		
		jtfSchedule = new JTextField();
		jtfSchedule.setColumns(10);
		jtfSchedule.setBounds(112, 142, 104, 20);
		panel.add(jtfSchedule);
		
		jtfDuration = new JTextField();
		jtfDuration.setColumns(10);
		jtfDuration.setBounds(131, 203, 104, 20);
		panel.add(jtfDuration);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"TD", "TP"}));
		comboBox.setBounds(112, 259, 104, 20);
		panel.add(comboBox);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frmCreateSessionsTD_TP.hide();
				new SessionChoice().frmSessionChoice.setVisible(true);
			}
		});
		btnCancel.setBackground(Color.LIGHT_GRAY);
		btnCancel.setFont(new Font("Calibri", Font.BOLD, 13));
		btnCancel.setBounds(24, 321, 77, 23);
		panel.add(btnCancel);
		
		JDateChooser dateChooser = new JDateChooser();
		dateChooser.setBounds(112, 87, 104, 20);
		panel.add(dateChooser);
		
		JPanel panel_1 = new JPanel();
		frmCreateSessionsTD_TP.getContentPane().add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblUnit = new JLabel("Unit ");
		lblUnit.setFont(new Font("Calibri", Font.BOLD, 16));
		lblUnit.setBounds(24, 34, 81, 14);
		panel_1.add(lblUnit);
		
		JLabel lblTeachersId = new JLabel("Teacher's ID");
		lblTeachersId.setFont(new Font("Calibri", Font.BOLD, 16));
		lblTeachersId.setBounds(24, 87, 92, 14);
		panel_1.add(lblTeachersId);
		
		JComboBox comboBox_1 = new JComboBox();
		ArrayList<Integer> teacherList = new TeacherDAO().getTeachersList();
		for(int i=0; i<teacherList.size(); i++)
			comboBox_1.addItem((Integer) teacherList.get(i));
		comboBox_1.setBounds(138, 84, 104, 20);
		panel_1.add(comboBox_1);
		
		JLabel lblReplacingTeachersId = new JLabel("Replacing Teacher's ID");
		lblReplacingTeachersId.setFont(new Font("Calibri", Font.BOLD, 16));
		lblReplacingTeachersId.setBounds(0, 125, 160, 14);
		panel_1.add(lblReplacingTeachersId);
		
		JComboBox comboBox_3 = new JComboBox();
		for(int i=0; i<teacherList.size(); i++)
			comboBox_3.addItem((Integer) teacherList.get(i));
		comboBox_3.setBounds(170, 122, 104, 20);
		panel_1.add(comboBox_3);
		
		JLabel lblGroupNumber = new JLabel("Group Number");
		lblGroupNumber.setFont(new Font("Calibri", Font.BOLD, 16));
		lblGroupNumber.setBounds(24, 167, 104, 14);
		panel_1.add(lblGroupNumber);
		
		JComboBox comboBox_2 = new JComboBox();
		ArrayList<String> studentGroupsList = new StudentsGroupDAO().getUnitGroups();
		for(int i=0; i<studentGroupsList.size(); i++)
			comboBox_2.addItem(studentGroupsList.get(i));
		comboBox_2.setBounds(138, 164, 104, 20);
		panel_1.add(comboBox_2);
		
		JLabel lblClassroom = new JLabel("Classroom");
		lblClassroom.setFont(new Font("Calibri", Font.BOLD, 16));
		lblClassroom.setBounds(24, 204, 81, 14);
		panel_1.add(lblClassroom);
		
		JLabel lblNbrAbsences = new JLabel("Nbr. absences");
		lblNbrAbsences.setFont(new Font("Calibri", Font.BOLD, 16));
		lblNbrAbsences.setBounds(24, 258, 104, 14);
		panel_1.add(lblNbrAbsences);
		
		jtfUnit = new JTextField();
		jtfUnit.setColumns(10);
		jtfUnit.setBounds(138, 31, 104, 20);
		panel_1.add(jtfUnit);
		
		jtfClassroom = new JTextField();
		jtfClassroom.setColumns(10);
		jtfClassroom.setBounds(138, 201, 104, 20);
		panel_1.add(jtfClassroom);
		
		jtfNbrAbsences = new JTextField("0");
		jtfNbrAbsences.setColumns(10);
		jtfNbrAbsences.setBounds(138, 255, 104, 20);
		panel_1.add(jtfNbrAbsences);
		
		JButton btnAdd = new JButton("add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
				String date = sdf.format(dateChooser.getDate());
				if(new SessionDAO().add(new Session(jtfSessionID.getText(), date, jtfSchedule.getText(), Integer.parseInt(jtfDuration.getText()), (String) comboBox.getSelectedItem(), jtfUnit.getText(), (int) comboBox_1.getSelectedItem(), (String) comboBox_2.getSelectedItem(), jtfClassroom.getText(), Integer.parseInt(jtfNbrAbsences.getText()), "undone"), null, (int) comboBox_3.getSelectedItem())==0)
					JOptionPane.showMessageDialog(frmCreateSessionsTD_TP, "Database Error !!!");
				else {
					JOptionPane.showMessageDialog(frmCreateSessionsTD_TP, "Successfully !!!");
				}
			}
		});
		btnAdd.setBackground(new Color(135, 206, 250));
		btnAdd.setFont(new Font("Calibri", Font.BOLD, 13));
		btnAdd.setBounds(10, 321, 77, 23);
		panel_1.add(btnAdd);
		
		JButton btnUpdate = new JButton("update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
				String date = sdf.format(dateChooser.getDate());
				if(new SessionDAO().update(new Session(jtfSessionID.getText(), date, jtfSchedule.getText(), Integer.parseInt(jtfDuration.getText()), (String) comboBox.getSelectedItem(), jtfUnit.getText(), (int) comboBox_1.getSelectedItem(), (String) comboBox_2.getSelectedItem(), jtfClassroom.getText(), Integer.parseInt(jtfNbrAbsences.getText()), "undone"), null, (int) comboBox_3.getSelectedItem())==0)
					JOptionPane.showMessageDialog(frmCreateSessionsTD_TP, "Database Error !!!\\n Verify if the session ID exists");
				else
					JOptionPane.showMessageDialog(frmCreateSessionsTD_TP, "Successfully !!!");
			}
		});
		btnUpdate.setBackground(new Color(255, 218, 185));
		btnUpdate.setFont(new Font("Calibri", Font.BOLD, 13));
		btnUpdate.setBounds(97, 321, 80, 23);
		panel_1.add(btnUpdate);
		
		JButton btnDelete = new JButton("delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
				String date = sdf.format(dateChooser.getDate());
				if(new SessionDAO().delete(new Session(jtfSessionID.getText(), date, jtfSchedule.getText(), Integer.parseInt(jtfDuration.getText()), (String) comboBox.getSelectedItem(), jtfUnit.getText(), (int) comboBox_1.getSelectedItem(), (String) comboBox_2.getSelectedItem(), jtfClassroom.getText(), Integer.parseInt(jtfNbrAbsences.getText()), "undone"))==0)
					JOptionPane.showMessageDialog(frmCreateSessionsTD_TP, "Database Error !!!\\n Verify if the session ID exists");
				else
					JOptionPane.showMessageDialog(frmCreateSessionsTD_TP, "Successfully !!!");
			}
		});
		btnDelete.setBackground(new Color(255, 51, 0));
		btnDelete.setFont(new Font("Calibri", Font.BOLD, 13));
		btnDelete.setBounds(187, 321, 77, 23);
		panel_1.add(btnDelete);
		
		JLabel lblPutTheSame = new JLabel("Put the same teacher'ID if there's no replacing");
		lblPutTheSame.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblPutTheSame.setForeground(Color.RED);
		lblPutTheSame.setBounds(10, 142, 254, 14);
		panel_1.add(lblPutTheSame);
	}
}
