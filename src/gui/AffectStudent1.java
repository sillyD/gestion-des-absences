package gui;

import java.awt.*;

import javax.swing.*;
import dao.*;
import java.awt.event.*;
/**
 * This is the first frame for affecting the student in groups
 * 
 * @author DIOP-SONY
 * @version 1.0
 */
public class AffectStudent1 {

	public JFrame frmAffectStudentsIn;
	private JTextField textField_1;
	// allows us to get the group number in the next frame
	public static String numGroup;

	/**
	 * Create the application.
	 */
	public AffectStudent1() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmAffectStudentsIn = new JFrame();
		frmAffectStudentsIn.getContentPane().setBackground(new Color(255, 255, 255));
		frmAffectStudentsIn.setTitle("AFFECT STUDENTS IN A GROUP");
		frmAffectStudentsIn.setBounds(100, 100, 389, 300);
		frmAffectStudentsIn.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmAffectStudentsIn.setLocationRelativeTo(null);
		frmAffectStudentsIn.getContentPane().setLayout(null);
		
		JLabel lblEnterTheGroup = new JLabel("Enter the group number");
		lblEnterTheGroup.setFont(new Font("Calibri", Font.BOLD, 20));
		lblEnterTheGroup.setBounds(10, 64, 255, 32);
		frmAffectStudentsIn.getContentPane().add(lblEnterTheGroup);
		
		textField_1 = new JTextField();
		textField_1.setBounds(221, 71, 142, 20);
		frmAffectStudentsIn.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmAffectStudentsIn.hide();
				new GroupManaging().frmGestionDeGroupes.setVisible(true);
			}
		});
		btnCancel.setFont(new Font("Calibri", Font.BOLD, 14));
		btnCancel.setBackground(new Color(192, 192, 192));
		btnCancel.setBounds(114, 173, 112, 23);
		frmAffectStudentsIn.getContentPane().add(btnCancel);
		
		JButton btnNext = new JButton("Next");
		btnNext.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(new StudentsGroupDAO().SearchGroup(textField_1.getText())==1) {
					numGroup = textField_1.getText();
					if(new StudentsGroupDAO().getGroupStaffing(numGroup)>=new StudentsGroupDAO().getStudentList(numGroup).length) {
						JOptionPane.showMessageDialog(frmAffectStudentsIn, "WARNING !!!\n the maximum capacity is reached for this group!\n tip: try to put some students in others groups");
						int diff = new StudentsGroupDAO().getGroupStaffing(numGroup)-new StudentsGroupDAO().getStudentList(numGroup).length;
						if (diff > 0)
							JOptionPane.showMessageDialog(frmAffectStudentsIn, "there is "+diff+" students who have to change group");
						frmAffectStudentsIn.hide();
						new AffectStudent2().frmAffectStudentsIn.setVisible(true);
					}
				}
				else
					JOptionPane.showMessageDialog(null, "This group doesn't exist !");
			}
		});
		btnNext.setFont(new Font("Calibri", Font.BOLD, 14));
		btnNext.setBackground(new Color(72, 209, 204));
		btnNext.setBounds(236, 173, 112, 23);
		frmAffectStudentsIn.getContentPane().add(btnNext);
		
		
		
	}
}
