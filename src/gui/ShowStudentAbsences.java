package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import dao.ConnectionDAO;
import dao.StudentDAO;
import dao.UnitDAO;
import net.proteanit.sql.DbUtils;

import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
/**
 * This is the frame used to a teacher to show the absences of a student in his sessions
 *  
 * @author DIOP-SONY
 * @version 1.0
 */
public class ShowStudentAbsences extends ConnectionDAO{

	public JFrame frmShowStudentAbsences;
	private JTable table;

	/**
	 * Create the application.
	 */
	public ShowStudentAbsences() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmShowStudentAbsences = new JFrame();
		frmShowStudentAbsences.setTitle("SHOW STUDENT ABSENCES");
		frmShowStudentAbsences.setBounds(100, 100, 546, 347);
		frmShowStudentAbsences.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmShowStudentAbsences.setLocationRelativeTo(null);
		frmShowStudentAbsences.getContentPane().setLayout(null);
		
		JLabel lblStudentsId = new JLabel("Student's ID");
		lblStudentsId.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 15));
		lblStudentsId.setBounds(29, 29, 93, 14);
		frmShowStudentAbsences.getContentPane().add(lblStudentsId);
		
		JComboBox studentsBox = new JComboBox();
		ArrayList<Integer> students = new StudentDAO().getStudentList();
		for(int i=0; i<students.size(); i++)
			studentsBox.addItem((int)students.get(i));
		studentsBox.setBounds(132, 26, 124, 20);
		frmShowStudentAbsences.getContentPane().add(studentsBox);
		
		JComboBox unitBox = new JComboBox();
		ArrayList<String> units = new UnitDAO().getUnits();
		for(int i=0; i<units.size(); i++)
			unitBox.addItem((String)units.get(i));
		unitBox.setBounds(132, 51, 124, 20);
		frmShowStudentAbsences.getContentPane().add(unitBox);
		
		JButton btnShow = new JButton("Show");
		btnShow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Connection con = DriverManager.getConnection(URL, LOGIN, PASS);
					PreparedStatement ps = con.prepareStatement("SELECT * FROM Absence WHERE idEtudiant = ? AND idSeance LIKE '%"+(String)unitBox.getSelectedItem()+"%'");
					ps.setInt(1, (int)studentsBox.getSelectedItem());
					ResultSet rs = ps.executeQuery();
					table.setModel(DbUtils.resultSetToTableModel(rs));
					table.setEnabled(false);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnShow.setBackground(new Color(135, 206, 235));
		btnShow.setFont(new Font("Calibri", Font.BOLD, 15));
		btnShow.setBounds(314, 45, 89, 23);
		frmShowStudentAbsences.getContentPane().add(btnShow);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(56, 91, 408, 139);
		frmShowStudentAbsences.getContentPane().add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmShowStudentAbsences.hide();
				new TeacherChoice().frmTeacherMenu.setVisible(true);
			}
		});
		btnCancel.setFont(new Font("Calibri", Font.BOLD, 15));
		btnCancel.setBackground(new Color(192, 192, 192));
		btnCancel.setBounds(29, 274, 89, 23);
		frmShowStudentAbsences.getContentPane().add(btnCancel);
		
		JLabel lblUnit = new JLabel("Unit");
		lblUnit.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 15));
		lblUnit.setBounds(56, 54, 61, 14);
		frmShowStudentAbsences.getContentPane().add(lblUnit);
	}

}
