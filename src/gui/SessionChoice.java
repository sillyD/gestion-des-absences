package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
/**
 * This is the frame used to choose a type of session we would create
 *  
 * @author DIOP-SONY
 * @version 1.0
 */
public class SessionChoice {

	public JFrame frmSessionChoice;

	/**
	 * Create the application.
	 */
	public SessionChoice() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmSessionChoice = new JFrame();
		frmSessionChoice.setTitle("SESSION CHOICE");
		frmSessionChoice.setBounds(100, 100, 432, 244);
		frmSessionChoice.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmSessionChoice.setLocationRelativeTo(null);
		frmSessionChoice.getContentPane().setLayout(null);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"TD-TP", "AMPHI-EXAM"}));
		comboBox.setBounds(57, 69, 119, 20);
		frmSessionChoice.getContentPane().add(comboBox);
		
		JButton btnOk = new JButton("OK");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if((String)comboBox.getSelectedItem()=="TD-TP") {
					frmSessionChoice.hide();
					new CreateSessionTD_TP().frmCreateSessionsTD_TP.setVisible(true);
				}
				else {
					frmSessionChoice.hide();
					new CreateSessionAMPHI_EXAM().frmCreateSessionamphiexam.setVisible(true);
				}
			}
		});
		btnOk.setBackground(new Color(135, 206, 235));
		btnOk.setFont(new Font("Calibri", Font.BOLD, 16));
		btnOk.setBounds(223, 68, 89, 23);
		frmSessionChoice.getContentPane().add(btnOk);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmSessionChoice.hide();
				new ManagerChoice().frmMenuDuGestionnaire.setVisible(true);
			}
		});
		btnCancel.setFont(new Font("Calibri", Font.BOLD, 16));
		btnCancel.setBackground(new Color(192, 192, 192));
		btnCancel.setBounds(317, 171, 89, 23);
		frmSessionChoice.getContentPane().add(btnCancel);
	}

}
