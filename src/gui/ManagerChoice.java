package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
/**
 * This is the frame used to show the choices of a manager
 *  
 * @author DIOP-SONY
 * @version 1.0
 */
public class ManagerChoice {

	public JFrame frmMenuDuGestionnaire;

	/**
	 * Create the application.
	 */
	public ManagerChoice() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmMenuDuGestionnaire = new JFrame();
		frmMenuDuGestionnaire.setTitle("Menu du Gestionnaire");
		frmMenuDuGestionnaire.setBounds(100, 100, 450, 300);
		frmMenuDuGestionnaire.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMenuDuGestionnaire.setLocationRelativeTo(null);
		frmMenuDuGestionnaire.getContentPane().setLayout(null);
		
		JLabel lblBienvenueDansLe = new JLabel("WELCOME TO THE MANAGER MENU");
		lblBienvenueDansLe.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 15));
		lblBienvenueDansLe.setBounds(98, 28, 241, 14);
		frmMenuDuGestionnaire.getContentPane().add(lblBienvenueDansLe);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Manage a unit, a teacher or student", "Manage a student group", "Quotas and absence types", "Absence management", "Create session(s)"}));
		comboBox.setBounds(131, 103, 260, 20);
		frmMenuDuGestionnaire.getContentPane().add(comboBox);
		
		JButton btnValider = new JButton("OK");
		btnValider.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				switch ((String) comboBox.getSelectedItem()) {
				
				case "Manage a unit, a teacher or student":
					frmMenuDuGestionnaire.hide();
					new UnitTeacherStudentManaging().frmGestionDeCours.setVisible(true);
					break;
					
				case "Manage a student group":
					frmMenuDuGestionnaire.hide();
					new GroupManaging().frmGestionDeGroupes.setVisible(true);
					break;
					
				case "Quotas and absence types":
					frmMenuDuGestionnaire.hide();
					new TypeQuotasAbsence().frmDefineAbsenceTypes.setVisible(true);
					break;
					
				case "Absence management":
					frmMenuDuGestionnaire.hide();
					new ManagementAbsences().frmManagementAbsences.setVisible(true);
					break;
					
				case "Create session(s)":
					frmMenuDuGestionnaire.hide();
					new SessionChoice().frmSessionChoice.setVisible(true);
					break;
					
				default:
					JOptionPane.showMessageDialog(frmMenuDuGestionnaire, "Choice not available!!!");
					break;
				}
			}
		});
		btnValider.setBackground(new Color(50, 205, 50));
		btnValider.setBounds(47, 206, 89, 23);
		frmMenuDuGestionnaire.getContentPane().add(btnValider);
		frmMenuDuGestionnaire.getRootPane().setDefaultButton(btnValider); // btnValider is activated when the enter key is pressed
		
		JButton btnSeDeconnecter = new JButton("Log out");
		btnSeDeconnecter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frmMenuDuGestionnaire.hide();
				new Homepage().frmAcceuil.setVisible(true);
			}
		});
		btnSeDeconnecter.setBackground(new Color(250, 128, 114));
		btnSeDeconnecter.setBounds(267, 206, 136, 23);
		frmMenuDuGestionnaire.getContentPane().add(btnSeDeconnecter);
	}

}
