package dao;

import java.sql.*;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import model.*;

/**
 * Allow access to the data in the table Seance
 * 
 * @author DIOP-SONY
 * @version 1.0
 */
public class SessionDAO extends ConnectionDAO{

	/**
	 * Constructor
	 */
	public SessionDAO() {
		super();
	}
	/**
	 * add a session in the database
	 * 
	 * @param session the session to be added
	 * @param studentsGroups the studentsGroups
	 * @param replacingTeacherID the replacing teacher
	 * @return the number of lines added
	 */
	public int add(Session session, String studentsGroups, int replacingTeacherID) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("INSERT INTO Seance(idSeance, idEnseignant, idEnseignantRemplacant, dateSeance, horaire, duree, typeSeance, nomCours, numGroupe, GroupesEtudiants, nomSalle, nbreAbsences, statut) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			ps.setString(1, session.getId());
			ps.setInt(2, session.getTeacherId());
			ps.setInt(3, replacingTeacherID);
			ps.setString(4, session.getDate());
			ps.setString(5, session.getTimetable());
			ps.setInt(6, session.getDuration());
			ps.setString(7, session.getType());
			ps.setString(8, session.getUnitName());
			ps.setString(9, session.getGroupNumber());
			ps.setString(10, studentsGroups);
			ps.setString(11, session.getClassroom());
			ps.setInt(12, session.getAbsencesNumber());
			ps.setString(13, session.getStatus());
			
			if(this.SearchClassroom(session.getClassroom())==0)
				this.addClass(session.getClassroom(), session.getType());
			returnValue = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			if(e.getMessage().contains("ORA-02291"))
				JOptionPane.showMessageDialog(null, "The unit, the teacher's ID or the group doesn't exist!!");
			else
				e.printStackTrace();
		}finally {
			
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	/**
	 * add a classroom in the database
	 * 
	 * @param name the class name
	 * @param type the class type
	 * @return the number of lines added
	 */
	private int addClass(String name, String type) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			if(type.equals("EXAM") || type.equals("AMPHI")) {
				ps = con.prepareStatement("INSERT INTO Salle(nomSalle, typeSalle) VALUES(?, ?)");
				ps.setString(1, name);
				ps.setString(2, "AMPHI");
			}
			
			returnValue = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	/**
	 * update a session in the database
	 * 
	 * @param session the session to update
	 * @param studentsGroups the studentsGroups
	 * @param replacingTeacherID the replacing teacher
	 * @return the number of lines updated
	 */
	public int update(Session session, String studentsGroups, int replacingTeacherID) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("UPDATE Seance SET idEnseignant = ?, idEnseignantRemplacant = ?, horaire = ?, duree = ?, typeSeance = ?, nomCours = ?, dateSeance = ?, numGroupe = ?, GroupesEtudiants = ?,  nomSalle = ?, nbreAbsences = ? WHERE idSeance = ?");
			ps.setInt(1, session.getTeacherId());
			ps.setInt(2, replacingTeacherID);
			ps.setString(3, session.getTimetable());
			ps.setInt(4, session.getDuration());
			ps.setString(5, session.getType());
			ps.setString(6, session.getUnitName());
			ps.setString(7, session.getDate());
			ps.setString(8, session.getGroupNumber());
			ps.setString(9, studentsGroups);
			ps.setString(10, session.getClassroom());
			ps.setInt(11, session.getAbsencesNumber());
			ps.setString(12, session.getId());
			
			if(this.SearchClassroom(session.getClassroom())==0)
				this.addClass(session.getClassroom(), session.getType());
			returnValue = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			if(e.getMessage().contains("ORA-02291"))
				JOptionPane.showMessageDialog(null, "The unit, the teacher's ID or the group doesn't exist!!");
			else
				e.printStackTrace();
		}finally {
			
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	/**
	 * allows to update to update the number of absences of a session
	 * 
	 * @param nbreAbsences the number of absences
	 * @param idSession the session's ID
	 * @return the number of lines updated
	 */
	public int updateNbreAbsences(int nbreAbsences, String idSession) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("UPDATE Seance SET nbreAbsences = ?, statut = ? WHERE idSeance = ?");
			ps.setInt(1, nbreAbsences);
			ps.setString(2, "done");
			ps.setString(3, idSession);
			
			returnValue = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			if(e.getMessage().contains("ORA-02291"))
				JOptionPane.showMessageDialog(null, "The unit, the teacher's ID or the group doesn't exist!!");
			else
				e.printStackTrace();
		}finally {
			
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	/**
	 * delete a session in the database
	 * 
	 * @param session the session to delete
	 * @return the number of lines deleted
	 */
	public int delete(Session session) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("DELETE Seance WHERE idSeance = ?");
			ps.setString(1, session.getId());
			
			returnValue = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	/**
	 * allows to verify if a classroom exists
	 * @param className the class name to verify
	 * @return the number of lines found
	 */
	private int SearchClassroom(String className) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("SELECT * FROM Salle WHERE nomSalle = ?");
			ps.setString(1, className);
			
			returnValue = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	/**
	 * replace a teacher in a session
	 * 
	 * @param sessionID the session
	 * @param replacingTeacherID the replacing teacher
	 * @return the number of lines updated
	 */
	public int ReplaceTeacher(String sessionID, int replacingTeacherID) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("UPDATE Seance SET idEnseignantRemplacant = ? WHERE idSeance = ?");
			ps.setInt(1, replacingTeacherID);
			ps.setString(2, sessionID);
			
			returnValue = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			if(e.getMessage().contains("ORA-02291"))
				JOptionPane.showMessageDialog(null, "this teacher(or the session) doesn't exist");
			else
				e.printStackTrace();
		}finally {
			
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	/**
	 * allows to get the date of a session
	 * 
	 * @param sessionID the session ID
	 * @return the date of the session
	 */
	public Date getDate(String sessionID) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Date returnValue = null;
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("SELECT * FROM Seance WHERE idSeance = ?");
			ps.setString(1, sessionID);
			
			rs = ps.executeQuery();
			if(rs.next())
				returnValue = rs.getDate("dateSeance");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			if(e.getMessage().contains("ORA-02291"))
				JOptionPane.showMessageDialog(null, "this teacher(or the session) doesn't exist");
			else
				e.printStackTrace();
		}finally {
			
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	/**
	 * allows to get all of the sessions undone
	 * @return the list of the undone sessions
	 */
	public ArrayList<String> getSessions() {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<String> sessionlist = new ArrayList<>();
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("SELECT * FROM Seance WHERE statut = 'undone'");
			
			rs = ps.executeQuery();
			while(rs.next())
				sessionlist.add(rs.getString("idSeance"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			if(e.getMessage().contains("ORA-02291"))
				JOptionPane.showMessageDialog(null, "this teacher(or the session) doesn't exist");
			else
				e.printStackTrace();
		}finally {
			
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return sessionlist;
	}
	/**
	 * get the exam sessions list
	 * @return the exam sessions list
	 */
	public ArrayList<String> getExamSessions() {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<String> examSessionlist = new ArrayList<>();
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("SELECT idSeance FROM Absence natural join Seance WHERE typeSeance = 'EXAM' AND justificatif is not null GROUP BY idSeance");
			
			rs = ps.executeQuery();
			while(rs.next())
				examSessionlist.add(rs.getString("idSeance"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			if(e.getMessage().contains("ORA-02291"))
				JOptionPane.showMessageDialog(null, "this teacher(or the session) doesn't exist");
			else
				e.printStackTrace();
		}finally {
			
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return examSessionlist;
	}
	/**
	 * allows to get the duration of a session
	 * 
	 * @param idSession the session
	 * @return the duration of the session
	 */
	public int getDuration(String idSession) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int duration = 0;
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("SELECT * FROM Seance WHERE idSeance = ?");
			ps.setString(1, idSession);
			
			rs = ps.executeQuery();
			if(rs.next())
				duration = rs.getInt("duree");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			if(e.getMessage().contains("ORA-02291"))
				JOptionPane.showMessageDialog(null, "this teacher(or the session) doesn't exist");
			else
				e.printStackTrace();
		}finally {
			
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return duration;
	}
}
