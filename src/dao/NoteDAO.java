package dao;

import java.sql.*;

/**
 * allows access to the table Note in the database
 * 
 * @author DIOP-SONY
 * @version 1.0
 *
 */
public class NoteDAO extends ConnectionDAO{

	/**
	 * constructor
	 */
	public NoteDAO() {
		super();
	}
	/**
	 * give the mark 0 to a student who was absent in exam
	 * 
	 * @param studentID the student concerned
	 * @param sessionID the session ID
	 * @param mark the student's mark
	 * @return the number of lines added
	 */
	public int giveZero(int studentID, String sessionID, int mark) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("INSERT INTO Note(idEtudiant, idSeance, note) VALUES (?, ?, ?)");
			ps.setInt(1, studentID);
			ps.setString(2, sessionID);
			ps.setInt(3, mark);
			
			returnValue = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			if(e.getMessage().contains("ORA-00001"))
				try {
					Thread.sleep(1);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			else
				e.printStackTrace();
		}finally {
			
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
}
