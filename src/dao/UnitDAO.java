package dao;

import java.sql.*;
import java.util.ArrayList;

import model.*;
/**
 * Class allowing access to the data in the database
 * 
 * @author DIOP-SONY
 * @version 1.0
 */
public class UnitDAO extends ConnectionDAO{

	/**
	 * Constructor
	 */
	public UnitDAO() {
		super();
	}
	
	/**
	 * add a unit in the table Cours
	 * 
	 * @param unit the unit to be added
	 * @return the number of lines added in the table
	 */
	public int add(Unit unit) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;
		
		// connection to the database
		try {

			// connection...
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("INSERT INTO Cours (nomCours, masseHoraire, heuresTD, heuresTP, heuresAMPHI, heuresEXAM) VALUES (?, ?, ?, ?, ?, ?)");
			ps.setString(1, unit.getName());
			ps.setInt(2, unit.getHourlyMass());
			ps.setInt(3, unit.getHoursTD());
			ps.setInt(4, unit.getHoursTP());
			ps.setInt(5, unit.getHoursAMPHI());
			ps.setInt(6, unit.getHoursEXAM());

			// Execution of the query
			returnValue = ps.executeUpdate();

		} catch (Exception e) {
			if (e.getMessage().contains("ORA-00001"))
				System.out.println("Ajout impossible !");
			else
				e.printStackTrace();
		} finally {
			// closing preparedStatement and connection
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	
	/**
	 * update a unit
	 * 
	 * @param unit the unit to be updated
	 * @return the number of lines updated
	 */
	public int update(Unit unit) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;

		// connection to the database
		try {

			//connection...
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("UPDATE Cours set nomCours = ?, masseHoraire = ?, heuresTD = ?, heuresTP = ?, heuresAMPHI = ?, heuresEXAM = ? WHERE nomCours = ?");
			ps.setString(1, unit.getName());
			ps.setInt(2, unit.getHourlyMass());
			ps.setInt(3, unit.getHoursTD());
			ps.setInt(4, unit.getHoursTP());
			ps.setInt(5, unit.getHoursAMPHI());
			ps.setInt(6, unit.getHoursEXAM());
			ps.setString(7, unit.getName());

			// Execution of the query
			returnValue = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// closing preparedStatement and connection
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	/**
	 * delete a unit
	 * 
	 * @param unit the unit to be deleted
	 * @return the number of lines deleted
	 */
	public int delete(Unit unit) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;

		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
	
			ps = con.prepareStatement("DELETE FROM Cours WHERE nomCours = ?");
			ps.setString(1, unit.getName());

			returnValue = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		}
	    finally {
			
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	/**
	 * allows to get the unit list of the database
	 * @return the unit list
	 */
	public ArrayList<String> getUnits() {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<String> unitList = new ArrayList<>();
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("SELECT * FROM Cours");
			rs = ps.executeQuery();
			
			while(rs.next())
				unitList.add(rs.getString("nomCours"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return unitList;
	}
}
