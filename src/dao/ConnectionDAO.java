package dao;
/**
 * Access class to the database
 * 
 * @author DIOP-SONY
 * @version 1.0
 */
public class ConnectionDAO {

	/**
	 * Connection parameters to the database
	 * URL, LOGIN and PASS are constants
	 */
	protected final static String URL   = "jdbc:oracle:thin:@localhost:1521:orcl";
	protected final static String LOGIN = "c##silly";   
	protected final static String PASS  = "aspirine"; 
	
	/**
	 * Constructor
	 */
	public ConnectionDAO() {
		// pilot loading..
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.err.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
		}
	}
}
