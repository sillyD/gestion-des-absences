package dao;

import java.sql.*;

/**
 * Access class to the data in the tables TypeAbsence and Quota
 * 
 * @author DIOP-SONY
 * @version 1.0
 */
public class TypeQuotasAbsencesDAO extends ConnectionDAO{

	/**
	 * Constructor
	 */
	public TypeQuotasAbsencesDAO() {
		super();
	}
	/**
	 * Allows to add an absence type 
	 * 
	 * @param AbsenceType the absence type to add
	 * @return the number of lines added
	 */
	public int addAbsenceType(String AbsenceType) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("INSERT INTO TypeAbsence(absence_type) VALUES(?)");
			ps.setString(1, AbsenceType);
			
			returnValue = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	/**
	 * allows to add absences quotas
	 * 
	 * @param threshold1 the first threshold
	 * @param threshold2 the second threshold
	 * @param threshold3 the third threshold
	 * @param threshold4 the fourth threshold
	 * @param threshold5 the fifth threshold
	 * @param threshold6 the sixth threshold
	 * @return the number of lines added
	 */
	public int addAbsenceQuotas(int threshold1, int threshold2, int threshold3, int threshold4, int threshold5, int threshold6) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("UPDATE Quota SET seuil1=?, seuil2=?, seuil3=?, seuil4=?, seuil5=?, seuil6=?");
			ps.setInt(1, threshold1);
			ps.setInt(2, threshold2);
			ps.setInt(3, threshold3);
			ps.setInt(4, threshold4);
			ps.setInt(5, threshold5);
			ps.setInt(6, threshold6);
			
			returnValue = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
}
