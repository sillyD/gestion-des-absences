package dao;

import java.sql.*;
import java.util.ArrayList;

import javax.swing.JOptionPane;

/**
 * Allows access to the data in the table of absences
 * 
 * @author DIOP-SONY
 * @version 1.0
 */
public class AbsenceDAO extends ConnectionDAO{

	/**
	 * constructor
	 */
	public AbsenceDAO() {
		super();
	}
	
	/**
	 * getter for the list of the absence types
	 * 
	 * @return the the list of the absence types
	 */
	public ArrayList<String> getAbsenceTypes(){
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<String> listAbsenceTypes = new ArrayList<>();
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("SELECT * FROM TypeAbsence");
			rs = ps.executeQuery();
			
			while(rs.next())
				listAbsenceTypes.add(rs.getString("absence_type"));
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return listAbsenceTypes;
	}
	/**
	 * getter for the absences ID of a student
	 * @param studentID the student concerned
	 * @return the absences list
	 */
	public ArrayList<Integer> getAbsences(int studentID) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Integer> absenceList = new ArrayList<>();
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("SELECT idAbsence FROM Absence WHERE idEtudiant = ? AND justificatif is null");
			ps.setInt(1, studentID);
			
			rs = ps.executeQuery();
			
			while(rs.next())
				absenceList.add(rs.getInt("idAbsence"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return absenceList;
	}
	/**
	 * get the list of the students whose absences are unexcused and justified
	 * @return the list of the students whose absences are unexcused and justified
	 */
	public ArrayList<Integer> getStudentListUnexcusedAndJustified() {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Integer> studentList = new ArrayList<>();
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("SELECT * FROM Absence natural join Seance WHERE typeSeance = 'EXAM' AND justificatif is not null AND etat = 'non excusee'");
			
			rs = ps.executeQuery();
			
			while(rs.next())
				studentList.add(rs.getInt("idEtudiant"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return studentList;
	}
	/**
	 * get the list of the students and session whose absences are unexcused and unjustified
	 * @return the list of the students whose absences are unexcused and unjustified
	 */
	public ArrayList<String> getStudentListUnexcusedAndUnjustified() {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<String> studentList = new ArrayList<>();
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("SELECT * FROM Absence natural join Seance WHERE typeSeance = 'EXAM' AND justificatif is null AND etat = 'non excusee'");
			
			rs = ps.executeQuery();
			
			while(rs.next())
				studentList.add(rs.getInt("idEtudiant")+"/"+rs.getString("idSeance"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return studentList;
	}
	/**
	 * get the proof of a student 
	 * 
	 * @param studentID the student ID
	 * @param sessionID the session ID
	 * @return the proof
	 */
	public String getProof(int studentID, String sessionID) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String proof = null;
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("SELECT * FROM Absence WHERE idSeance = ? AND idEtudiant = ?");
			ps.setString(1, sessionID);
			ps.setInt(2, studentID);
			
			rs = ps.executeQuery();
			
			if(rs.next())
				proof = rs.getString("justificatif");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return proof;
	}
	/**
	 * add an absence in the database
	 * 
	 * @param absenceType the absence's type
	 * @param IdStudent the student's id
	 * @param idSession the session's id
	 * @return the number of lines added
	 */
	public int add(String absenceType, int IdStudent, String idSession) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("INSERT INTO Absence(idSeance, idEtudiant, idAbsence, typeAbsence, etat) VALUES (?, ?, ABSENCE_SEQ.nextval, ?, ?)");
			ps.setString(1, idSession);
			ps.setInt(2, IdStudent);
			ps.setString(3, absenceType);
			ps.setString(4, "non excusee");
			
			returnValue = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			if(e.getMessage().contains("ORA-02291"))
				JOptionPane.showMessageDialog(null, "This session doesn't exist!!!");
			else if(e.getMessage().contains("ORA-00001"))
				JOptionPane.showMessageDialog(null, "The student "+IdStudent+" has already been registered!!!");
			else 
				e.printStackTrace();
		}finally {
			
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	/**
	 * allows to excuse absences
	 * 
	 * @param sessionID the session ID
	 * @param studentID the student ID
	 * @return the number of lines updated
	 */
	public int excuseAbsence(String sessionID, int studentID) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("UPDATE Absence SET etat = ? WHERE idSeance = ? AND idEtudiant = ?");
			ps.setString(1, "excusee");
			ps.setString(2, sessionID);
			ps.setInt(3, studentID);
			
			returnValue = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block	
			e.printStackTrace();
		}finally {
			
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	
	/**
	 * add an absence in the database (anticipate an absence)
	 * 
	 * @param absenceType the absence's type
	 * @param IdStudent the student's id
	 * @param idSession the session's id
	 * @param proof the absence proof
	 * @return the number of lines added
	 */
	public int add(String absenceType, int IdStudent, String idSession, String proof) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("INSERT INTO Absence(idSeance, idEtudiant, idAbsence, typeAbsence, justificatif, etat) VALUES (?, ?, ABSENCE_SEQ.nextval, ?, ?, ?)");
			ps.setString(1, idSession);
			ps.setInt(2, IdStudent);
			ps.setString(3, absenceType);
			ps.setString(4, proof);
			ps.setString(5, "non excusee");
			
			returnValue = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			if(e.getMessage().contains("ORA-02291"))
				JOptionPane.showMessageDialog(null, "This session doesn't exist!!!");
			else if(e.getMessage().contains("ORA-00001"))
				JOptionPane.showMessageDialog(null, "The student "+IdStudent+" has already been registered!!!");
			else 
				e.printStackTrace();
		}finally {
			
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	/**
	 * allows to add a proof for an absence
	 * @param proof the proof to be added
	 * @param absenceID the absence ID
	 * @return the number of lines updated
	 */
	public int updateProof(String proof, int absenceID) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("UPDATE Absence SET justificatif = ? WHERE idAbsence = ?");
			ps.setString(1, proof);
			ps.setInt(2, absenceID);
			
			returnValue = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block	
			e.printStackTrace();
		}finally {
			
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
}
