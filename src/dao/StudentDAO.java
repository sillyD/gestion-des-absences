package dao;

import java.sql.*;
import java.util.ArrayList;

import model.*;

/**
 * class allowing access to the data in the database
 * 
 * @author DIOP-SONY
 * @version 1.0
 */
public class StudentDAO extends ConnectionDAO {

	/**
	 * Constructor
	 */
	public StudentDAO() {
		super();
	}
	
	/**
	 * add a student in the database
	 * 
	 * @param student the student to be added
	 * @return the number of lines added
	 */
	public int add(Student student) {
		Connection con = null;
		PreparedStatement ps1 = null;
		int returnValue = 0;
		
		// connection to the database
		try {

			// connection...
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps1 = con.prepareStatement("INSERT INTO Etudiant (idEtudiant, nom, prenom, mail, pass, filiere, numGroupeCours, numGroupeAnglais, numGroupeExam, nbreHeuresAbsences) VALUES (ETUDIANT_SEQ.nextval, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
								
			ps1.setString(1, student.getLastName());
			ps1.setString(2, student.getFirstName());
			ps1.setString(3, student.getMail());
			ps1.setString(4, student.getPassword());
			ps1.setString(5, student.getSector());
			ps1.setString(6, student.getUnitGroupNumber());
			ps1.setString(7, student.getEnglishGroupNumber());
			ps1.setString(8, student.getExamGroupNumber());
			ps1.setInt(9, 0);
		
			// query execution
			returnValue = ps1.executeUpdate();
			this.addInUser(student.getMail(), student.getPassword(), "student");
			
			
		} catch (Exception e) {
			if (e.getMessage().contains("ORA-00001"))
				System.out.println("Ajout impossible !");
			else
				e.printStackTrace();
		} finally {
			// closing preparedStatement and connection
			try {
				if (ps1 != null) {
					ps1.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	/**
	 * Allows to add a user in the table Utilisateurs
	 * 
	 * @param mail email of the user
	 * @param pass user's password
	 * @param usertype category of the user
	 * @return the number of lines added
	 */
	private int addInUser(String mail, String pass, String usertype) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;
		int id = this.getId(mail);
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("INSERT INTO Utilisateurs(id, mail, pass, typeUtilisateur) VALUES(?, ?, ?, ?)");
			ps.setInt(1, id);
			ps.setString(2, mail);
			ps.setString(3, pass);
			ps.setString(4, usertype);
			
			returnValue = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			// closing preparedStatement and connection
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	/**
	 * allows to get the id using the email
	 * 
	 * @param mail user's email
	 * @return the number of lines found
	 */
	public int getId(String mail) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int returnValue = 0;
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("SELECT idEtudiant FROM Etudiant WHERE mail = ?");
			ps.setString(1, mail);
			
			
			rs = ps.executeQuery();
			
			if(rs.next())
				returnValue = rs.getInt("idEtudiant");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			// closing preparedStatement and connection
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	
	/**
	 * update a student
	 * 
	 * @param student the student to be updated
	 * @return the number of lines updated
	 */
	public int update(Student student) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;
		
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
		
			ps = con.prepareStatement("UPDATE Etudiant set nom = ?, prenom = ?, pass = ?, filiere = ?, numGroupeCours = ?, numGroupeAnglais = ?, numGroupeExam = ?, nbreHeuresAbsences = ? WHERE mail = ?");
			ps.setString(1, student.getLastName());
			ps.setString(2, student.getFirstName());
			ps.setString(3, student.getPassword());
			ps.setString(4, student.getSector());
			ps.setString(5, student.getUnitGroupNumber());
			ps.setString(6, student.getEnglishGroupNumber());
			ps.setString(7, student.getExamGroupNumber());
			ps.setInt(8, student.getAbsencesHours());
			ps.setString(9, student.getMail());

			returnValue = ps.executeUpdate();
			this.UpdateInUser(student.getMail(), student.getPassword(), "student");

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	/**
	 * allows to update a user in the table Utilisateurs 
	 * 
	 * @param mail user's email
	 * @param pass user's password 
	 * @param usertype user's category
	 * @return the number of lines updated
	 */
	private int UpdateInUser(String mail, String pass, String usertype) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;
		int id = this.getId(mail);
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("UPDATE Utilisateurs SET mail = ?, pass = ? where id = ?");
			ps.setString(1, mail);
			ps.setString(2, pass);
			ps.setInt(3, id);
			
			returnValue = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			// closing preparedStatement and connection
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	/**
	 * allows to add the number of absence hours of the student
	 * 
	 * @param absenceHours the absences hours
	 * @param studentID the student's ID
	 * @return the number of lines updated
	 */
	public int updateNbrAbsencesHours(int absenceHours, int studentID, String status) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			if(status.equals("unexcused")) {
				ps = con.prepareStatement("UPDATE Etudiant SET nbreHeuresAbsences = ? where idEtudiant = ?");
				ps.setInt(1,this.getNbreAbsenceHours(studentID)+absenceHours);
				ps.setInt(2, studentID);
			}
			else {
				ps = con.prepareStatement("UPDATE Etudiant SET nbreHeuresAbsences = ? where idEtudiant = ?");
				ps.setInt(1,this.getNbreAbsenceHours(studentID)-absenceHours);
				ps.setInt(2, studentID);
			}
			
			returnValue = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			// closing preparedStatement and connection
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	/**
	 * allows to get the number of hours absence
	 * 
	 * @param studentID the student concerned
	 * @return the number of hours missed
	 */
	public int getNbreAbsenceHours(int studentID) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int nbreAbsenceHours = 0;
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("SELECT * FROM Etudiant where idEtudiant = ?");
			ps.setInt(1, studentID);
			
			rs = ps.executeQuery();
			
			if(rs.next())
				nbreAbsenceHours = rs.getInt("nbreHeuresAbsences");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			// closing preparedStatement and connection
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return nbreAbsenceHours;
	}
	/**
	 * Delete a student
	 * 
	 * @param student the student to be deleted
	 * @return the number of lines deleted 
	 */
	public int delete(Student student) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;

		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
		
			ps = con.prepareStatement("DELETE FROM Etudiant WHERE mail = ?");
			ps.setString(1, student.getMail());

			returnValue = ps.executeUpdate();
			this.DeleteInUser(student.getMail());

		} catch (Exception e) {
			e.printStackTrace();
		}
	    finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	/**
	 * Allows to delete a user in the table Utilisateurs
	 * 
	 * @param mail user's email
	 * @return the number of lines deleted
	 */
	private int DeleteInUser(String mail) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("DELETE FROM Utilisateurs WHERE mail = ?");
			ps.setString(1, mail);
			
			returnValue = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			// closing preparedStatement and connection
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	/**
	 * update a student adding a group number
	 * 
	 * @param groupNumber the group number
	 * @param studentID the student ID
	 * @return the number of lines updated
	 */
	public int updateNumGroupe(String groupNumber, int studentID) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;
		
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
		
			if (groupNumber.contains("UN")==true) {
				ps = con.prepareStatement("UPDATE Etudiant set numGroupeCours = ? WHERE idEtudiant = ?");
				ps.setString(1, groupNumber);
				ps.setInt(2, studentID);
			}
			else if (groupNumber.contains("EN")==true) {
				ps = con.prepareStatement("UPDATE Etudiant set numGroupeAnglais = ? WHERE idEtudiant = ?");
				ps.setString(1, groupNumber);
				ps.setInt(2, studentID);
			}
			else {
				ps = con.prepareStatement("UPDATE Etudiant set numGroupeExam = ? WHERE idEtudiant = ?");
				ps.setString(1, groupNumber);
				ps.setInt(2, studentID);
			}

			returnValue = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	
	/**
	 * getter for the planning
	 * 
	 * @param identifier the student identifier
	 * @return the path of the planning
	 */
	public String getPlanning(String identifier) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String returnValue = null;
		
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			if(identifier.contains("esigelec")) {
				ps = con.prepareStatement("SELECT planning FROM Etudiant INNER JOIN GroupeEtudiants ON Etudiant.numGroupeCours = GroupeEtudiants.numGroupe WHERE mail = ?");
				ps.setString(1, identifier);
			}
			else {
				ps = con.prepareStatement("SELECT planning FROM Etudiant INNER JOIN GroupeEtudiants ON Etudiant.numGroupeCours = GroupeEtudiants.numGroupe WHERE idEtudiant = ?");
				ps.setInt(1, Integer.parseInt(identifier));
			}

			rs = ps.executeQuery();
			
			if (rs.next())
				returnValue = rs.getString("planning");

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	/**
	 * allows to get the list of the students who don't belong to the group entered in parameter
	 * 
	 * @param numGroup the groupNumber
	 * @return the list of the students
	 */
	public ArrayList<String> getListeEleve(String numGroup){
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<String> studentList = new ArrayList<>();
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			if(numGroup.contains("UN")) {
				ps = con.prepareStatement("SELECT * FROM Etudiant WHERE numGroupeCours != ? or numGroupeCours is null");
				ps.setString(1, numGroup);
			}
			else if(numGroup.contains("EN")) {
				ps = con.prepareStatement("SELECT * FROM Etudiant WHERE numGroupeAnglais != ? or numGroupeAnglais is null");
				ps.setString(1, numGroup);
			}
			else {
				ps = con.prepareStatement("SELECT * FROM Etudiant WHERE numGroupeExam != ? or numGroupeExam is null");
				ps.setString(1, numGroup);
			}
			
			rs = ps.executeQuery();
			
			while(rs.next())
				studentList.add(rs.getString("nom")+"-"+rs.getString("prenom")+"-"+rs.getInt("idEtudiant"));
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return studentList;
	}
	/**
	 * getter of all of the students
	 * @return the list of the students
	 */
	public ArrayList<Integer> getStudentList() {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Integer> studentList = new ArrayList<>();
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("SELECT * FROM Etudiant");
			
			rs = ps.executeQuery();
			
			while(rs.next())
				studentList.add(rs.getInt("idEtudiant"));
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return studentList;
	}
	
	/**
	 * getter of all of the students with their absences hours
	 * @return the list of the students with their absences hours
	 */
	public ArrayList<String> getStudentListWithAbsenceHours() {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<String> studentList = new ArrayList<>();
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("SELECT * FROM Etudiant");
			
			rs = ps.executeQuery();
			
			while(rs.next())
				studentList.add(rs.getInt("idEtudiant")+"/"+rs.getInt("nbreHeuresAbsences"));
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return studentList;
	}
}
