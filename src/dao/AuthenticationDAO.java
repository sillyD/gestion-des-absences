package dao;

import java.sql.*;

/**
 * Verification class
 * 
 * @author DIOP-SONY
 * @version 1.0
 */
public class AuthenticationDAO extends ConnectionDAO {

	/**
	 * Constructor
	 */
	public AuthenticationDAO() {
		super();
	}
	
	/**
	 * allows the verification of the username and password
	 * 
	 * @param identifier the username
	 * @param password the password
	 * @return the type of the user who's trying to connect
	 */
	public String verify(String identifier, String password) {
		
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String usertype = null;
		
		// connection to the database
		try {		
			// connection...
			con = DriverManager.getConnection(URL, LOGIN, PASS);		
			
			if(identifier.contains("esigelec")==true) {
				ps = con.prepareStatement("SELECT * FROM Utilisateurs WHERE mail = ? AND pass = ?");
				ps.setString(1, identifier);
				ps.setString(2, password);
			}
			else {
				ps = con.prepareStatement("SELECT * FROM Utilisateurs WHERE id = ? AND pass = ?");
				ps.setInt(1, Integer.parseInt(identifier));
				ps.setString(2, password);
			}
			// Query execution
			rs = ps.executeQuery();
			
			if(rs.next())
				usertype = rs.getString("typeUtilisateur");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			// closing preparedStatement and connection
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return usertype;
	}	
}

