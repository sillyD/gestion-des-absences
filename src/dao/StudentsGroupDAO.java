package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.StudentsGroup;

/**
 * Class allowing access to the data in table GroupeEtudiants
 * 
 * @author DIOP-SONY
 * @version 1.0
 */
public class StudentsGroupDAO extends ConnectionDAO{

	/**
	 * Constructor
	 */
	public StudentsGroupDAO() {
		super();
	}
	
	/**
	 * allow adding of a student group in the table GroupeEtudiants
	 * 
	 * @param studentsGroup the student group to be added
	 * @return the number of lines added in the table
	 */
	public int add(StudentsGroup studentsGroup) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;
		
		// connection to the database
		try {

			// connection...
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("INSERT INTO GroupeEtudiants (numGroupe, typeGroupe, planning) VALUES (?, ?, ?)");
			ps.setString(1, studentsGroup.getNumber());
			ps.setString(2, studentsGroup.getType());
			ps.setString(3, studentsGroup.getPlanning());

			// Execution of the query
			returnValue = ps.executeUpdate();

		} catch (Exception e) {
			if (e.getMessage().contains("ORA-00001"))
				System.out.println("Ajout impossible !");
			else
				e.printStackTrace();
		} finally {
			// closing preparedStatement and connection
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	
	/**
	 * allows updating of a student group in the table GroupeEtudiants
	 * 
	 * @param studentsGroup the student to be updated
	 * @return the number of lines updated in the table
	 */
	public int update(StudentsGroup studentsGroup) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;

		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("UPDATE GroupeEtudiants set typeGroupe = ?, planning = ?  WHERE numGroupe = ?");
			ps.setString(1, studentsGroup.getType());
			ps.setString(2, studentsGroup.getPlanning());
			ps.setString(3, studentsGroup.getNumber());

			returnValue = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
	
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	/**
	 * allows deleting a student group from the table GroupeEtudiants
	 * 
	 * @param studentsGroup the student to be deleted 
	 * @return return the number of lines deleted in the table
	 */
	public int delete(StudentsGroup studentsGroup) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;

		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
	
			ps = con.prepareStatement("DELETE FROM GroupeEtudiants WHERE numGroupe = ?");
			ps.setString(1, studentsGroup.getNumber());

			returnValue = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		}
	    finally {
			
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	/**
	 * allows us to verify if a group exists using its number
	 * 
	 * @param numGroup the group to be tested
	 * @return the number of lines found in the table
	 */
	public int SearchGroup(String numGroup) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("SELECT * FROM GroupeEtudiants WHERE numGroupe = ?");
			ps.setString(1, numGroup);
			
			returnValue = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	/**
	 * allows to get the student list of a group
	 * @param groupNumber the group number
	 * @return the student list
	 */
	public String[] getStudentList(String groupNumber) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String[] studentList = null;
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("SELECT nom, prenom, idEtudiant FROM Etudiant WHERE numGroupeCours = ? ORDER BY nom");
			ps.setString(1, groupNumber);
			
			rs = ps.executeQuery();
			
			int i = 0; // adding the students in the array by increment
			studentList = new String[this.getGroupStaffing(groupNumber)];
			while(rs.next()) {
				studentList[i] = rs.getString("nom")+"/"+rs.getString("prenom")+"/"+rs.getString("idEtudiant");
				i++;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return studentList;
	}
	/**
	 * allows to get the staff of a group
	 * @param groupNumber the group number
	 * @return the staff of the group
	 */
	public int getGroupStaffing(String groupNumber) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int returnValue = 0;
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			if(groupNumber.contains("UN")) {
				ps = con.prepareStatement("SELECT COUNT(*) FROM Etudiant WHERE numGroupeCours = ?");
				ps.setString(1, groupNumber);
			}
			else if(groupNumber.contains("EN")) {
				ps = con.prepareStatement("SELECT COUNT(*) FROM Etudiant WHERE numGroupeAnglais = ?");
				ps.setString(1, groupNumber);
			}
			else {
				ps = con.prepareStatement("SELECT COUNT(*) FROM Etudiant WHERE numGroupeExam = ?");
				ps.setString(1, groupNumber);
			}
			
			rs = ps.executeQuery();
			
			if(rs.next())
				returnValue = rs.getInt("COUNT(*)");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	/**
	 * allows to get all of the unit groups
	 * @return the list of the groups
	 */
	public ArrayList<String> getUnitGroups() {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<String> groupsList = new ArrayList<>();
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("SELECT * FROM GroupeEtudiants");
			
			rs = ps.executeQuery();
			
			while(rs.next()) {
				if (rs.getString("numGroupe").contains("UN"))
					groupsList.add(rs.getString("numGroupe"));
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return groupsList;
	}
	
	/**
	 * allows to get all of the groups
	 * @return the list of the groups
	 */
	public ArrayList<String> getGroups() {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<String> groupsList = new ArrayList<>();
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("SELECT * FROM GroupeEtudiants");
			
			rs = ps.executeQuery();
			
			while(rs.next()) {
				groupsList.add(rs.getString("numGroupe"));
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return groupsList;
	}
}
