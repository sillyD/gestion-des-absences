package dao;

import java.sql.*;

/**
 * allows access to the data in the table QUOTA
 * 
 * @author DIOP-SONY
 * @version 1.0
 */
public class QuotaDAO extends ConnectionDAO{

	/**
	 * Constructor
	 */
	public QuotaDAO() {
		super();
	}
	/**
	 * this method allows to verify if the student exceeds the first quota
	 * 
	 * @param absencesHours the absences hours of the student
	 * @return the difference between the absences hours and the first quota
	 */
	public int verifyFirstQuota(int absencesHours) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int returnValue = 0;
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("SELECT * FROM Quota");
			rs = ps.executeQuery();
			
			if(rs.next())
				returnValue = absencesHours-rs.getInt("seuil1");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	
	/**
	 * this method allows to verify if the student exceeds the second quota
	 * 
	 * @param absencesHours the absences hours of the student
	 * @return the difference between the absences hours and the second quota
	 */
	public int verifySecondQuota(int absencesHours) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int returnValue = 0;
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("SELECT * FROM Quota");
			rs = ps.executeQuery();
			
			if(rs.next())
				returnValue = absencesHours-rs.getInt("seuil2");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	
	/**
	 * this method allows to verify if the student exceeds the third quota
	 * 
	 * @param absencesHours the absences hours of the student
	 * @return the difference between the absences hours and the third quota
	 */
	public int verifyThirdQuota(int absencesHours) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int returnValue = 0;
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("SELECT * FROM Quota");
			rs = ps.executeQuery();
			
			if(rs.next())
				returnValue = absencesHours-rs.getInt("seuil3");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	
	/**
	 * this method allows to verify if the student exceeds the fourth quota
	 * 
	 * @param absencesHours the absences hours of the student
	 * @return the difference between the absences hours and the fourth quota
	 */
	public int verifyFourthQuota(int absencesHours) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int returnValue = 0;
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("SELECT * FROM Quota");
			rs = ps.executeQuery();
			
			if(rs.next())
				returnValue = absencesHours-rs.getInt("seuil4");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	
	/**
	 * this method allows to verify if the student exceeds the fifth quota
	 * 
	 * @param absencesHours the absences hours of the student
	 * @return the difference between the absences hours and the fifth quota
	 */
	public int verifyFifthQuota(int absencesHours) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int returnValue = 0;
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("SELECT * FROM Quota");
			rs = ps.executeQuery();
			
			if(rs.next())
				returnValue = absencesHours-rs.getInt("seuil5");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	
	/**
	 * this method allows to verify if the student exceeds the sixth quota
	 * 
	 * @param absencesHours the absences hours of the student
	 * @return the difference between the absences hours and the sixth quota
	 */
	public int verifySixthQuota(int absencesHours) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int returnValue = 0;
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("SELECT * FROM Quota");
			rs = ps.executeQuery();
			
			if(rs.next())
				returnValue = absencesHours-rs.getInt("seuil6");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
}
