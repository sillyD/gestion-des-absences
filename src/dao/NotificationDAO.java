package dao;

import java.sql.*;

import javax.swing.JOptionPane;

/**
 * used to send informations to the student
 * 
 * @author DIOP-SONY
 * @version 1.0
 */
public class NotificationDAO extends ConnectionDAO{

	/**
	 * constructor
	 */
	public NotificationDAO() {
		super();
	}
	/**
	 * used to trigger catch-ups for the student
	 * 
	 * @param studentID the student concerned
	 * @param message the message to be sent
	 * @return the number of lines added
	 */
	public int sendMessage(int studentID, String message) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("INSERT INTO Notifications(idEtudiant, message, dateEnvoi) VALUES(?, ?, SYSDATE)");
			ps.setInt(1, studentID);
			ps.setString(2, message);
			
			returnValue = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			if(e.getMessage().contains("ORA-00001"))
				JOptionPane.showMessageDialog(null, "Already done !!!");
			else
				e.printStackTrace();
		}finally {
			
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
}
