package dao;

import java.sql.*;
import java.util.ArrayList;

import model.*;

/**
 * Access class to the data in the table 
 * 
 * @author DIOP-SONY
 * @version 1.0
 */
public class TeacherDAO extends ConnectionDAO {

	/**
	 * Constructor
	 */
	public TeacherDAO() {
		super();
	}
	
	/**
	 * add a teacher in the database
	 * 
	 * @param teacher the teacher to be added
	 * @param planning the planning of the teacher
	 * @return the number of lines added
	 */
	public int add(Teacher teacher, String planning) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;
		
		// connection to the database
		try {

			// connection...
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("INSERT INTO Enseignant (idEnseignant, nom, prenom, mail, pass, telephone, planning) VALUES (ENSEIGNANT_SEQ.nextval, ?, ?, ?, ?, ?, ?)");
			ps.setString(1, teacher.getLastName());
			ps.setString(2, teacher.getFirstName());
			ps.setString(3, teacher.getMail());
			ps.setString(4, teacher.getPassword());
			ps.setString(5, teacher.getPhone());
			ps.setString(6, planning);

			// Query execution
			returnValue = ps.executeUpdate();
			this.addInUser(teacher.getMail(), teacher.getPassword(), "teacher");

		} catch (Exception e) {
			if (e.getMessage().contains("ORA-00001"))
				System.out.println("Ajout impossible !");
			else
				e.printStackTrace();
		} finally {
			// closing preparedStatement and connection
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	/**
	 * Allows to add a user in the table Utilisateurs
	 * 
	 * @param mail email of the user
	 * @param pass user's password
	 * @param usertype category of the user
	 * @return the number of lines added
	 */
	private int addInUser(String mail, String pass, String usertype) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;
		int id = this.getId(mail);
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("INSERT INTO Utilisateurs(id, mail, pass, typeUtilisateur) VALUES(?, ?, ?, ?)");
			ps.setInt(1, id);
			ps.setString(2, mail);
			ps.setString(3, pass);
			ps.setString(4, usertype);
			
			returnValue = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			// closing preparedStatement and connection
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	/**
	 * allows to get the id using the email
	 * 
	 * @param mail user's email
	 * @return the number of lines found
	 */
	private int getId(String mail) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int returnValue = 0;
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("SELECT idEnseignant FROM Enseignant WHERE mail = ?");
			ps.setString(1, mail);
			
			
			rs = ps.executeQuery();
			
			if(rs.next())
				returnValue = rs.getInt("idEnseignant");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			// closing preparedStatement and connection
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	/**
	 * update a teacher
	 * 
	 * @param teacher the teacher to be updated
	 * @param planning the teacher's planning
	 * @return the number of lines updated
	 */
	public int update(Teacher teacher, String planning) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;
	
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
		
			ps = con.prepareStatement("UPDATE Enseignant set nom = ?, prenom = ?, pass = ?, telephone = ?, planning = ? WHERE mail = ?");
			ps.setString(1, teacher.getLastName());
			ps.setString(2, teacher.getFirstName());
			ps.setString(3, teacher.getPassword());
			ps.setString(4, teacher.getPhone());
			ps.setString(5, planning);
			ps.setString(6, teacher.getMail());

			returnValue = ps.executeUpdate();
			this.UpdateInUser(teacher.getMail(), teacher.getPassword(), "teacher");

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	/**
	 * allows to update a user in the table Utilisateurs 
	 * 
	 * @param mail user's email
	 * @param pass user's password 
	 * @param usertype user's category
	 * @return the number of lines updated
	 */
	private int UpdateInUser(String mail, String pass, String usertype) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;
		int id = this.getId(mail);
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("UPDATE Utilisateurs SET mail = ?, pass = ? where id = ?");
			ps.setString(1, mail);
			ps.setString(2, pass);
			ps.setInt(3, id);
			
			returnValue = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			// closing preparedStatement and connection
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	/**
	 * Delete a teacher
	 * 
	 * @param teacher the teacher to be deleted
	 * @return the number of lines deleted
	 */
	public int delete(Teacher teacher) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;

		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
		
			ps = con.prepareStatement("DELETE FROM Enseignant WHERE mail = ?");
			ps.setString(1, teacher.getMail());

			returnValue = ps.executeUpdate();
			this.DeleteInUser(teacher.getMail());

		} catch (Exception e) {
			e.printStackTrace();
		}
	    finally {
	
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	/**
	 * Allows to delete a user in the table Utilisateurs
	 * 
	 * @param mail user's email
	 * @return the number of lines deleted
	 */
	private int DeleteInUser(String mail) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
			ps = con.prepareStatement("DELETE FROM Utilisateurs WHERE mail = ?");
			ps.setString(1, mail);
			
			returnValue = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			// closing preparedStatement and connection
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	/**
	 * allows to get the planning of a teacher
	 * 
	 * @param identifier the teacher concerned
	 * @return the planning of the teacher
	 */
	public String getPlanning(String identifier) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String planning = null;
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			if(identifier.contains("esigelec")) {
				ps = con.prepareStatement("SELECT * FROM Enseignant WHERE mail = ?");
				ps.setString(1, identifier);
			}
			else {
				ps = con.prepareStatement("SELECT * FROM Enseignant WHERE idEnseignant = ?");
				ps.setInt(1, Integer.parseInt(identifier));
			}
			rs = ps.executeQuery();
			
			if(rs.next())
				planning = rs.getString("planning");
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			// closing preparedStatement and connection
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return planning;
	}
	/**
	 *allows to get the teacher's list of the database
	 * @return the teacher's list of the database
	 */
	public ArrayList<Integer> getTeachersList() {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Integer> teacherList = new ArrayList<>();
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);

			ps = con.prepareStatement("SELECT * FROM Enseignant");
			
			rs = ps.executeQuery();
			
			while(rs.next())
				teacherList.add(rs.getInt("idEnseignant"));
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			// closing preparedStatement and connection
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return teacherList;
	}
}

