CREATE TABLE Salle
(
	nomSalle VARCHAR2(20) CONSTRAINT NN_nomSalle NOT NULL,
	typeSalle VARCHAR2(10) CONSTRAINT CK_typeSalle CHECK (typeSalle IN ('TD', 'TP', 'AMPHI')),
	CONSTRAINT PK_Salle PRIMARY KEY(nomSalle)
);

CREATE TABLE Capacite
(
	typeGroupe VARCHAR2(10) CONSTRAINT CK_typeGroupe CHECK (typeGroupe IN ('unit', 'english', 'exam')),
	capMax INTEGER,
	CONSTRAINT PK_Capacite PRIMARY KEY(typeGroupe)
);

CREATE TABLE Cours
(
	nomCours VARCHAR2(50),
	masseHoraire INTEGER,
	heuresTD INTEGER, 
	heuresTP INTEGER, 
	heuresAMPHI INTEGER, 
	heuresEXAM INTEGER, 
	CONSTRAINT PK_Cours PRIMARY KEY(nomCours)
);

CREATE TABLE Quota
(
	seuil1 INTEGER,
	seuil2 INTEGER,
	seuil3 INTEGER,
	seuil4 INTEGER,
	seuil5 INTEGER,
	seuil6 INTEGER
);

CREATE TABLE TypeAbsence
(
	absence_type VARCHAR2(50)
);


CREATE TABLE Gestionnaire
(
	idGestionnaire INTEGER,
	nom VARCHAR2(50) CONSTRAINT NN_nom NOT NULL,
	prenom VARCHAR2(50) CONSTRAINT NN_prenom NOT NULL,
	mail VARCHAR2(50) CONSTRAINT NN_mail NOT NULL,
	pass VARCHAR2(50) CONSTRAINT NN_pass NOT NULL,
	telephone VARCHAR2(50),
	CONSTRAINT PK_Gestionnaire PRIMARY KEY(idGestionnaire),
	CONSTRAINT UN_mail_password UNIQUE(mail, pass)
);

CREATE TABLE Enseignant
(
	idEnseignant INTEGER,
	nom VARCHAR2(50),
	prenom VARCHAR2(50),
	mail VARCHAR2(50),
	pass VARCHAR2(50),
	telephone VARCHAR2(50),
	planning VARCHAR2(100),
	CONSTRAINT PK_Enseignant PRIMARY KEY(idEnseignant),
	CONSTRAINT UN_mail UNIQUE(mail),
	CONSTRAINT UN_pass UNIQUE(pass)
);
CREATE SEQUENCE enseignant_seq
	START WITH     1
	INCREMENT BY   1
	NOCACHE
	NOCYCLE;

CREATE TABLE GroupeEtudiants
(
	numGroupe VARCHAR2(30),
	typeGroupe VARCHAR2(10),
	planning VARCHAR2(100),
	CONSTRAINT PK_GroupeEtudiants PRIMARY KEY(numGroupe),
	CONSTRAINT FK_typeGroupe FOREIGN KEY(typeGroupe) REFERENCES Capacite(typeGroupe) ON DELETE CASCADE
);

CREATE TABLE Etudiant
(
	idEtudiant INTEGER,
	nom VARCHAR2(50),
	prenom VARCHAR2(50),
	mail VARCHAR2(50),
	pass VARCHAR2(50),
	filiere VARCHAR2(50),
	numGroupeCours VARCHAR2(50),
	numGroupeAnglais VARCHAR2(50),
	numGroupeExam VARCHAR2(50),
	nbreHeuresAbsences INTEGER,
	CONSTRAINT PK_Etudiant PRIMARY KEY(idEtudiant),
	CONSTRAINT FK_numGroupeCours FOREIGN KEY(numGroupeCours) REFERENCES GroupeEtudiants(numGroupe) ON DELETE CASCADE,
	CONSTRAINT FK_numGroupeAnglais FOREIGN KEY(numGroupeAnglais) REFERENCES GroupeEtudiants(numGroupe) ON DELETE CASCADE,
	CONSTRAINT FK_numGroupeExam FOREIGN KEY(numGroupeExam) REFERENCES GroupeEtudiants(numGroupe) ON DELETE CASCADE,
	CONSTRAINT UN_mailEtu UNIQUE(mail),
	CONSTRAINT UN_passEtu UNIQUE(pass)
);
CREATE SEQUENCE etudiant_seq
	START WITH     1
	INCREMENT BY   1
	NOCACHE
	NOCYCLE;

CREATE TABLE Seance
(
	idSeance VARCHAR2(100),
	idEnseignant INTEGER,
	idEnseignantRemplacant INTEGER,
	dateSeance DATE CONSTRAINT NN_dateSeance NOT NULL,
	horaire VARCHAR2(50),
	duree INTEGER,
	typeSeance VARCHAR2(10),
	nomCours VARCHAR2(50),
	numGroupe VARCHAR2(50),
	GroupesEtudiants VARCHAR2(100),
	nomSalle VARCHAR2(50),
	nbreAbsences INTEGER,
	statut VARCHAR2(20),
	CONSTRAINT PK_Seance PRIMARY KEY(idSeance),
	CONSTRAINT FK_nomCours FOREIGN KEY(nomCours) REFERENCES Cours(nomCours) ON DELETE CASCADE,
	CONSTRAINT FK_numGroupe FOREIGN KEY(numGroupe) REFERENCES GroupeEtudiants(numGroupe) ON DELETE CASCADE,
	CONSTRAINT FK_idEnseignant FOREIGN KEY(idEnseignant) REFERENCES Enseignant(idEnseignant) ON DELETE CASCADE,
	CONSTRAINT FK_idEnseignantRemplacant FOREIGN KEY(idEnseignantRemplacant) REFERENCES Enseignant(idEnseignant) ON DELETE CASCADE,
	CONSTRAINT FK_nomSalle FOREIGN KEY(nomSalle) REFERENCES Salle(nomSalle) ON DELETE CASCADE
);

CREATE TABLE Absence
(
	idSeance VARCHAR2(100),
	idEtudiant INTEGER,
	idAbsence INTEGER,
	typeAbsence VARCHAR2(60),
	justificatif VARCHAR2(200),
	etat VARCHAR2(20),
	CONSTRAINT PK_Absence PRIMARY KEY(idSeance, idEtudiant),
	CONSTRAINT FK_idSeance FOREIGN KEY(idSeance) REFERENCES Seance(idSeance) ON DELETE CASCADE,
	CONSTRAINT FK_idEtudiant FOREIGN KEY(idEtudiant) REFERENCES Etudiant(idEtudiant) ON DELETE CASCADE
);
CREATE SEQUENCE absence_seq
	START WITH     1
	INCREMENT BY   1
	NOCACHE
	NOCYCLE;

CREATE TABLE Note
(
	idEtudiant INTEGER,
	idSeance VARCHAR2(100),
	note INTEGER,
	CONSTRAINT PK_Note PRIMARY KEY(idEtudiant, idSeance)
);

CREATE TABLE Notifications
(
	idEtudiant INTEGER,
	message VARCHAR2(200),
	dateEnvoi DATE,
	CONSTRAINT PK_Notifications PRIMARY KEY(idEtudiant, message)
);

CREATE TABLE Utilisateurs
(
	id INTEGER,
	mail VARCHAR2(100),
	pass VARCHAR2(100),
	typeUtilisateur VARCHAR2(50),
	CONSTRAINT PK_Utilisateurs PRIMARY KEY(id)
);